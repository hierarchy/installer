#
# This will split up each Rain library to its own github repo
#

./git-subsplit.sh init git@bitbucket.org:hierarchy/core.git
./git-subsplit.sh publish --no-tags src/Support:git@bitbucket.org:hierarhcy/support.git
./git-subsplit.sh publish --no-tags src/Stylist:git@bitbucket.org:hierarhcy/stylist.git
./git-subsplit.sh publish --no-tags src/Packages:git@bitbucket.org:hierarhcy/packages.git
./git-subsplit.sh publish --no-tags src/Media:git@bitbucket.org:hierarhcy/media.git
./git-subsplit.sh publish --no-tags src/Foundation:git@bitbucket.org:hierarhcy/foundation.git
./git-subsplit.sh publish --no-tags src/Console:git@bitbucket.org:hierarhcy/console.git
./git-subsplit.sh publish --no-tags src/Auth:git@bitbucket.org:hierarhcy/auth.git
rm -rf .subsplit/