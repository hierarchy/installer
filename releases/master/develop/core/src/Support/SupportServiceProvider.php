<?php

namespace Hierarchy\Alexa\Support;

use Illuminate\Support\ServiceProvider;

class SupportServiceProvider extends ServiceProvider
{

    public function register()
    {
        require_once __DIR__ . '/helpers.php';
    }
}
