<?php

use Illuminate\Contracts\View\Factory as ViewFactory;

/**
 * Helpers hierarchy
 */
if (!function_exists('theme')) {

    function theme($view = null, $data = [], $mergeData = [])
    {
        $factory = app(ViewFactory::class);

        if (func_num_args() === 0) {
            return $factory;
        }

        return $factory->make($view, $data, $mergeData);
    }
}

if (!function_exists('formatdate')) {
    function formatdate($date, $format = 'Y-m-d H:i:s')
    {
        $date = strtotime($date);

        return date($format, $date);
    }
}

if (!function_exists('formatSizeUnits')) {

    function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }
}

if (!function_exists('ext2icon')) {

    function ext2icon($ext, $list_icon = [])
    {
        if (empty($list_icon)) {
            $list_icon = [
                /*image*/
                '<i class="icon file image outline"></i>' => ['jpg', 'png', 'jpeg'],
                /*audio*/
                '<i class="icon file audio outline"></i>' => ['mp2', 'mp3', 'ogg', 'mid', 'm4a'],
                /*video*/
                '<i class="icon file video outline"></i>' => ['mp4', 'mpeg', 'avi', 'mkv', '3gp'],
                /*pdf*/
                '<i class="icon file pdf outline"></i>' => ['pdf'],
                /*chart*/
                '<i class="icon bar chart outline"></i>' => ['chart'],
                /*excel*/
                '<i class="icon file excel outline"></i>' => ['xls', 'xlsx'],
                /*docs*/
                '<i class="icon file word outline"></i>' => ['doc', 'docx'],
                /*persentasi*/
                '<i class="icon file powerpoint outline"></i>' => ['ppt', 'pptx'],
                /*zip*/
                '<i class="icon file archive outline"></i>' => ['zip', 'rar', '7z', 'tar.gz', 'iso'],
            ];
        }
        $icon = false;
        foreach ($list_icon as $key => $value) {
            if (in_array(strtolower($ext), $value)) {
                $icon = $key;
            }
        }

        return (!$icon) ? '<i class="icon file"></i>' : $icon;
    }
}

if (!function_exists('arrayToTable')) {
    function arrayToTable($array, $attribute = [])
    {
        /**
        <?php if (count($array) > 0): ?>
        <table class="">
        <thead>
        <tr>
        <th><?php echo implode('</th><th>', array_keys(current($array))); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($array as $row): array_map('htmlentities', $row); ?>
        <tr>
        <td><?php echo implode('</td><td>', $row); ?></td>
        </tr>
        <?php endforeach; ?>
        </tbody>
        </table>
        <?php endif; ?>
         */
    }
}
