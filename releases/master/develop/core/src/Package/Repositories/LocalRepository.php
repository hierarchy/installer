<?php

namespace Hierarchy\Alexa\Package\Repositories;

use Hierarchy\Alexa\Package\Repositories\Repository;

class LocalRepository extends Repository
{
    /**
     * Update cached repository of module information.
     *
     * @return bool
     */
    public function optimize()
    {
        $cachePath = $this->getCachePath();
        $cache = $this->getCache();
        $basenames = $this->getAllBasenames();
        $pacakges = collect();
        $basenames->each(function ($pacakge, $key) use ($pacakges, $cache) {
            $temp = collect($cache->get($pacakge));
            $manifest = collect($this->getManifest($pacakge));
            $pacakges->put($pacakge, $temp->merge($manifest));
        });
        $pacakges->each(function ($pacakge) {
            if (!$pacakge->has('enabled')) {
                $pacakge->put('enabled', true);
            }
            if (!$pacakge->has('order')) {
                $pacakge->put('order', 9001);
            }
            return $pacakge;
        });
        $content = json_encode($pacakges->all(), JSON_PRETTY_PRINT);
        return $this->files->put($cachePath, $content);
    }
    /**
     * Get all pacakges.
     *
     * @return Collection
     */
    public function all()
    {
        return $this->getCache()->sortBy('order');
    }
    /**
     * Get all module slugs.
     *
     * @return Collection
     */
    public function slugs()
    {
        $slugs = collect();
        $this->all()->each(function ($item, $key) use ($slugs) {
            $slugs->push($item['slug']);
        });
        return $slugs;
    }
    /**
     * Get pacakges based on where clause.
     *
     * @param  string  $key
     * @param  mixed   $value
     * @return Collection
     */
    public function where($key, $value)
    {
        return $this->all()->where($key, $value);
    }
    /**
     * Sort pacakges by given key in ascending order.
     *
     * @param  string  $key
     * @return Collection
     */
    public function sortBy($key)
    {
        $collection = $this->all();
        return $collection->sortBy($key);
    }
    /**
     * Sort pacakges by given key in ascending order.
     *
     * @param  string  $key
     * @return Collection
     */
    public function sortByDesc($key)
    {
        $collection = $this->all();
        return $collection->sortByDesc($key);
    }
    /**
     * Determines if the given module exists.
     *
     * @param  string  $slug
     * @return bool
     */
    public function exists($slug)
    {
        return $this->slugs()->contains(strtolower($slug));
    }
    /**
     * Returns count of all pacakges.
     *
     * @return int
     */
    public function count()
    {
        return $this->all()->count();
    }
    /**
     * Get a module property value.
     *
     * @param  string $property
     * @param  mixed  $default
     * @return mixed
     */
    public function get($property, $default = null)
    {
        list($slug, $key) = explode('::', $property);
        $pacakge = $this->where('slug', $slug);
        return $pacakge->get($key, $default);
    }
    /**
     * Set the given module property value.
     *
     * @param  string  $property
     * @param  mixed   $value
     * @return bool
     */
    public function set($property, $value)
    {
        list($slug, $key) = explode('::', $property);
        $cachePath = $this->getCachePath();
        $cache = $this->getCache();
        $pacakge = $this->where('slug', $slug);
        $pacakgeKey = $pacakge->keys()->first();
        $values = $pacakge->first();
        if (isset($values[$key])) {
            unset($values[$key]);
        }
        $values[$key] = $value;
        $pacakge = collect([$pacakgeKey => $values]);
        $merged = $cache->merge($pacakge);
        $content = json_encode($merged->all(), JSON_PRETTY_PRINT);
        return $this->files->put($cachePath, $content);
    }
    /**
     * Get all enabled pacakges.
     *
     * @return Collection
     */
    public function enabled()
    {
        return $this->all()->where('enabled', true);
    }
    /**
     * Get all disabled pacakges.
     *
     * @return Collection
     */
    public function disabled()
    {
        return $this->all()->where('enabled', false);
    }
    /**
     * Check if specified module is enabled.
     *
     * @param  string $slug
     * @return bool
     */
    public function isEnabled($slug)
    {
        $pacakge = $this->where('slug', $slug)
            ->first();
        return ($pacakge['enabled'] === true);
    }
    /**
     * Check if specified module is disabled.
     *
     * @param  string $slug
     * @return bool
     */
    public function isDisabled($slug)
    {
        $pacakge = $this->where('slug', $slug)
            ->first();
        return ($pacakge['enabled'] === false);
    }
    /**
     * Enables the specified module.
     *
     * @param  string $slug
     * @return bool
     */
    public function enable($slug)
    {
        return $this->set($slug . '::enabled', true);
    }
    /**
     * Disables the specified module.
     *
     * @param  string $slug
     * @return bool
     */
    public function disable($slug)
    {
        return $this->set($slug . '::enabled', false);
    }
    /**
     * Get the contents of the cache file.
     *
     * The cache file lists all module slugs and their
     * enabled or disabled status. This can be used to
     * filter out pacakges depending on their status.
     *
     * @return Collection
     */
    public function getCache()
    {
        $cachePath = $this->getCachePath();
        if (!$this->files->exists($cachePath)) {
            $content = json_encode(array(), JSON_PRETTY_PRINT);
            $this->files->put($cachePath, $content);
            $this->optimize();
            return collect(json_decode($content, true));
        }
        return collect(json_decode($this->files->get($cachePath), true));
    }
    /**
     * Get the path to the cache file.
     *
     * @return string
     */
    protected function getCachePath()
    {
        return storage_path('app/hipackage.json');
    }

    // /**
    //  * Update cached repository of package information.
    //  *
    //  * @return bool
    //  */
    // public function optimize()
    // {
    //     $cachePath = $this->getCachePath();
    //     $cache = $this->getCache();
    //     $basenames = $this->getAllBasenames();
    //     $packages = collect();
    //     $basenames->each(function ($package, $key) use ($packages, $cache) {
    //         $temp = collect($cache->get($package));
    //         $manifest = collect($this->getManifest($package));
    //         $packages->put($package, $temp->merge($manifest));
    //     });
    //     $packages->each(function ($package) {
    //         if (!$package->has('enabled')) {
    //             $package->put('enabled', true);
    //         }
    //         if (!$package->has('order')) {
    //             $package->put('order', 9001);
    //         }
    //         return $package;
    //     });
    //     $content = json_encode($packages->all(), JSON_PRETTY_PRINT);
    //     return $this->files->put($cachePath, $content);
    // }

    // /**
    //  * Get all package.
    //  *
    //  * @return Collection
    //  */
    // public function all()
    // {
    //     $basenames = $this->getAllBasenames();
    //     $packages = collect();

    //     $basenames->each(function ($package, $key) use ($packages) {
    //         $packages->put($package, $this->getProperties($package));
    //     });

    //     return $packages->sortBy('order');
    // }

    // /**
    //  * Get all package slugs.
    //  *
    //  * @return Collection
    //  */
    // public function slugs()
    // {
    //     $slugs = collect();

    //     $this->all()->each(function ($item, $key) use ($slugs) {
    //         $slugs->push($item['slug']);
    //     });

    //     return $slugs;
    // }

    // /**
    //  * Get Package based on where clause.
    //  *
    //  * @param  string  $key
    //  * @param  mixed   $value
    //  * @return Collection
    //  */
    // public function where($key, $value)
    // {
    //     $collection = $this->all();

    //     return $collection->where($key, $value);
    // }

    // /**
    //  * Sort Package by given key in ascending order.
    //  *
    //  * @param  string  $key
    //  * @return Collection
    //  */
    // public function sortBy($key)
    // {
    //     $collection = $this->all();

    //     return $collection->sortBy($key);
    // }

    // /**
    //  * Sort Package by given key in ascending order.
    //  *
    //  * @param  string  $key
    //  * @return Collection
    //  */
    // public function sortByDesc($key)
    // {
    //     $collection = $this->all();

    //     return $collection->sortByDesc($key);
    // }

    // /**
    //  * Determines if the given package exists.
    //  *
    //  * @param  string  $slug
    //  * @return bool
    //  */
    // public function exists($slug)
    // {
    //     return $this->slugs()->contains(strtolower($slug));
    // }

    // /**
    //  * Returns count of all package.
    //  *
    //  * @return int
    //  */
    // public function count()
    // {
    //     return $this->all()->count();
    // }

    // /**
    //  * Get a package's properties.
    //  *
    //  * @param  string $slug
    //  * @return Collection|null
    //  */
    // public function getProperties($slug)
    // {
    //     if (!is_null($slug)) {

    //         $package = studly_case($slug);
    //         $path = $this->getManifestPath($package);
    //         $contents = $this->files->get($path);
    //         $collection = collect(json_decode($contents, true));

    //         if (!$collection->has('order')) {
    //             $collection->put('order', 9001);
    //         }

    //         return $collection;
    //     }

    //     return null;
    // }

    // /**
    //  * Get a package property value.
    //  *
    //  * @param  string $property
    //  * @param  mixed  $default
    //  * @return mixed
    //  */
    // public function getProperty($property, $default = null)
    // {
    //     list($package, $key) = explode('::', $property);

    //     return $this->getProperties($package)->get($key, $default);
    // }

    // /**
    //  * Set the given package property value.
    //  *
    //  * @param  string  $property
    //  * @param  mixed   $value
    //  * @return bool
    //  */
    // public function setProperty($property, $value)
    // {
    //     list($package, $key) = explode('::', $property);

    //     $package = strtolower($package);
    //     $content = $this->getProperties($package);

    //     if (isset($content[$key])) {
    //         unset($content[$key]);
    //     }

    //     $content[$key] = $value;
    //     $content = json_encode($content, JSON_PRETTY_PRINT);

    //     return $this->files->put($this->getManifestPath($package), $content);
    // }

    // /**
    //  * Get all enabled package.
    //  *
    //  * @return Collection
    //  */
    // public function enabled()
    // {
    //     $packageCache = $this->getCache();

    //     $package = $this->all()->map(function ($item, $key) use ($packageCache) {
    //         $item['enabled'] = $packageCache->get($item['slug']);

    //         return $item;
    //     });

    //     return $package->where('enabled', true);
    // }

    // /**
    //  * Get all disabled package.
    //  *
    //  * @return Collection
    //  */
    // public function disabled()
    // {
    //     $packageCache = $this->getCache();

    //     $package = $this->all()->map(function ($item, $key) use ($packageCache) {
    //         $item['enabled'] = $packageCache->get($item['slug']);

    //         return $item;
    //     });

    //     return $package->where('enabled', false);
    // }

    // /**
    //  * Check if specified package is enabled.
    //  *
    //  * @param  string $slug
    //  * @return bool
    //  */
    // public function isEnabled($slug)
    // {
    //     $packageCache = $this->getCache();

    //     return $packageCache->get($slug) === true;
    // }

    // /**
    //  * Check if specified package is disabled.
    //  *
    //  * @param  string $slug
    //  * @return bool
    //  */
    // public function isDisabled($slug)
    // {
    //     $packageCache = $this->getCache();

    //     return $packageCache->get($slug) === false;
    // }

    // /**
    //  * Enables the specified package.
    //  *
    //  * @param  string $slug
    //  * @return bool
    //  */
    // public function enable($slug)
    // {
    //     return $this->setCache($slug, true);
    // }

    // /**
    //  * Disables the specified package.
    //  *
    //  * @param  string $slug
    //  * @return bool
    //  */
    // public function disable($slug)
    // {
    //     return $this->setCache($slug, false);
    // }

    // /**
    //  * Refresh the cache with any newly found package.
    //  *
    //  * @return bool
    //  */
    // public function cache()
    // {
    //     $cacheFile = $this->getCachePath();
    //     $cache = $this->getCache();
    //     $package = $this->all();

    //     $collection = collect([]);

    //     foreach ($package as $package) {
    //         $collection->put($package['slug'], true);
    //     }

    //     $keys = $collection->keys()->toArray();
    //     $merged = $collection->merge($cache)->only($keys);
    //     $content = json_encode($merged->all(), JSON_PRETTY_PRINT);

    //     return $this->files->put($cacheFile, $content);
    // }

    // /**
    //  * Get the contents of the cache file.
    //  *
    //  * The cache file lists all package slugs and their
    //  * enabled or disabled status. This can be used to
    //  * filter out Package depending on their status.
    //  *
    //  * @return Collection
    //  */
    // public function getCache()
    // {
    //     $cacheFile = $this->getCachePath();

    //     if (!$this->files->exists($cacheFile)) {
    //         $package = $this->all();
    //         $content = [];

    //         foreach ($package as $package) {
    //             $content[$package['slug']] = true;
    //         }

    //         $content = json_encode($content, JSON_PRETTY_PRINT);

    //         $this->files->put($cacheFile, $content);

    //         return collect(json_decode($content, true));
    //     }

    //     return collect(json_decode($this->files->get($cacheFile), true));
    // }

    // /**
    //  * Set the given cache key value.
    //  *
    //  * @param  string  $key
    //  * @param  mixed  $value
    //  * @return int
    //  */
    // public function setCache($key, $value)
    // {
    //     $cacheFile = $this->getCachePath();
    //     $content = $this->getCache();

    //     $content->put($key, $value);

    //     $content = json_encode($content, JSON_PRETTY_PRINT);

    //     return $this->files->put($cacheFile, $content);
    // }

    // /**
    //  * Get the path to the cache file.
    //  *
    //  * @return string
    //  */
    // protected function getCachePath()
    // {
    //     return storage_path('app/hipackage.json');
    // }
}
