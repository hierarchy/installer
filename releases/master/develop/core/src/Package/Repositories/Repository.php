<?php

namespace Hierarchy\Alexa\Package\Repositories;

use Hierarchy\Alexa\Package\Contracts\RepositoryInterface;
use Illuminate\Config\Repository as Config;
use Illuminate\Filesystem\Filesystem;

abstract class Repository implements RepositoryInterface
{
    /**
     * @var \Illuminate\Config\Repository
     */
    protected $config;

    /**
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    /**
     * @var string $path Path to the defined packages directory
     */
    protected $path;

    /**
     * Constructor method.
     *
     * @param \Illuminate\Config\Repository      $config
     * @param \Illuminate\Filesystem\Filesystem  $files
     */
    public function __construct(Config $config, Filesystem $files)
    {
        $this->config = $config;
        $this->files = $files;
    }

    /**
     * Get all package basenames
     *
     * @return array
     */
    protected function getAllBasenames()
    {
        $path = $this->getPath();
        try {
            $collection = collect($this->files->directories($path));
            $basenames = $collection->map(function ($item, $key) {
                return basename($item);
            });
            return $basenames;
        } catch (\InvalidArgumentException $e) {
            return collect(array());
        }
    }

    /**
     * Get a package's manifest contents.
     *
     * @param  string $slug
     * @return Collection|null
     */
    public function getManifest($slug)
    {
        if (!is_null($slug)) {
            $package = studly_case($slug);
            $path = $this->getManifestPath($package);
            $contents = $this->files->get($path);
            $collection = collect(json_decode($contents, true));
            return $collection;
        }
        return null;
    }

    /**
     * Get packages path.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path ?: $this->config->get('package.path');
    }

    /**
     * Set packages path in "RunTime" mode.
     *
     * @param  string $path
     * @return object $this
     */
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     * Get path for the specified package.
     *
     * @param  string $slug
     * @return string
     */
    public function getpackagePath($slug)
    {
        $package = studly_case($slug);
        return $this->getPath() . "/{$package}/";
    }

    /**
     * Get path of package manifest file.
     *
     * @param  string $package
     * @return string
     */
    protected function getManifestPath($slug)
    {
        return $this->getpackagePath($slug) . 'Current/hipackage.json';
    }

    /**
     * Get packages namespace.
     *
     * @return string
     */
    public function getNamespace()
    {
        return rtrim($this->config->get('package.namespace'), '/\\');
    }
}
