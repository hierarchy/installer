<?php

namespace Hierarchy\Alexa\Package;

use Hierarchy\Alexa\Package\Contracts\RepositoryInterface;
use Illuminate\Foundation\Application;

/**
 *
 */
class Package implements RepositoryInterface
{
    /**
     * @var Application
     */
    protected $app;

    /**
     * @var RepositoryInterface
     */
    protected $repository;

    /**
     * Create a new packages instance.
     *
     * @param Application  $app
     * @param RepositoryInterface  $repository
     */
    public function __construct(Application $app, RepositoryInterface $repository)
    {
        $this->app = $app;
        $this->repository = $repository;
    }

    /**
     * Register the package service provider file from all packages.
     *
     * @return mixed
     */
    public function register()
    {
        $packages = $this->repository->enabled();
        $packages->each(function ($properties, $slug) {
            $this->registerServiceProvider($properties);
            $this->autoloadFiles($properties);
        });
    }

    /**
     * Register the package service provider.
     *
     * @param  string $properties
     * @return string
     * @throws \HiSoft\Package\Exception\FileMissingException
     */
    protected function registerServiceProvider($properties)
    {
        $namespace = $this->resolveNamespace($properties);
        $file = $this->repository->getPath() . "/{$namespace}/Current/Providers/{$namespace}ServiceProvider.php";
        if (is_file($file)) {
            $serviceProvider = $this->repository->getNamespace() . "\\" . $namespace . "\\Current\\Providers\\{$namespace}ServiceProvider";
            $this->app->register($serviceProvider);
        }
    }

    /**
     * Autoload custom package files.
     *
     * @param array  $properties
     * @return void
     */
    protected function autoloadFiles($properties)
    {
        if (isset($properties['autoload'])) {
            $namespace = $this->resolveNamespace($properties);
            $path = $this->repository->getPath() . "/{$namespace}/Current/";

            foreach ($properties['autoload'] as $file) {
                include $path . $file;
            }
        }
    }

    /**
     * [optimize description]
     * @return [type] [description]
     */
    public function optimize()
    {
        return $this->repository->optimize();
    }

    /**
     * Get all packages.
     *
     * @return Collection
     */
    public function all()
    {
        return $this->repository->all();
    }

    /**
     * Get all package slugs.
     *
     * @return array
     */
    public function slugs()
    {
        return $this->repository->slugs();
    }

    /**
     * Get packages based on where clause.
     *
     * @param  string  $key
     * @param  mixed   $value
     * @return Collection
     */
    public function where($key, $value)
    {
        return $this->repository->where($key, $value);
    }

    /**
     * Sort packages by given key in ascending order.
     *
     * @param  string  $key
     * @return Collection
     */
    public function sortBy($key)
    {
        return $this->repository->sortBy($key);
    }

    /**
     * Sort packages by given key in ascending order.
     *
     * @param  string  $key
     * @return Collection
     */
    public function sortByDesc($key)
    {
        return $this->repository->sortByDesc($key);
    }

    /**
     * Check if the given package exists.
     *
     * @param  string  $slug
     * @return bool
     */
    public function exists($slug)
    {
        return $this->repository->exists($slug);
    }

    /**
     * Returns count of all packages.
     *
     * @return int
     */
    public function count()
    {
        return $this->repository->count();
    }

    /**
     * Get packages path.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->repository->getPath();
    }

    /**
     * Set packages path in "RunTime" mode.
     *
     * @param  string $path
     * @return object $this
     */
    public function setPath($path)
    {
        return $this->repository->setPath($path);
    }

    /**
     * Get path for the specified package.
     *
     * @param  string $slug
     * @return string
     */
    public function getpackagePath($slug)
    {
        return $this->repository->getpackagePath($slug);
    }

    /**
     * Get packages namespace.
     *
     * @return string
     */
    public function getNamespace()
    {
        return $this->repository->getNamespace();
    }

    /**
     * Get a package's properties.
     *
     * @param  string $slug
     * @return mixed
     */
    public function getManifest($slug)
    {
        return $this->repository->getManifest($slug);
    }

    /**
     * Get a package property value.
     *
     * @param  string $property
     * @param  mixed  $default
     * @return mixed
     */
    public function get($property, $default = null)
    {
        return $this->repository->get($property, $default);
    }

    /**
     * Set a package property value.
     *
     * @param  string $property
     * @param  mixed  $value
     * @return bool
     */
    public function set($property, $value)
    {
        return $this->repository->set($property, $value);
    }

    /**
     * Gets all enabled packages.
     *
     * @return array
     */
    public function enabled()
    {
        return $this->repository->enabled();
    }

    /**
     * Gets all disabled packages.
     *
     * @return array
     */
    public function disabled()
    {
        return $this->repository->disabled();
    }

    /**
     * Check if specified package is enabled.
     *
     * @param  string $slug
     * @return bool
     */
    public function isEnabled($slug)
    {
        return $this->repository->isEnabled($slug);
    }

    /**
     * Check if specified package is disabled.
     *
     * @param  string $slug
     * @return bool
     */
    public function isDisabled($slug)
    {
        return $this->repository->isDisabled($slug);
    }

    /**
     * Enables the specified package.
     *
     * @param  string $slug
     * @return bool
     */
    public function enable($slug)
    {
        return $this->repository->enable($slug);
    }

    /**
     * Disables the specified package.
     *
     * @param  string $slug
     * @return bool
     */
    public function disable($slug)
    {
        return $this->repository->disable($slug);
    }

    /**
     * Resolve the correct package namespace.
     *
     * @param array  $properties
     */
    private function resolveNamespace($properties)
    {
        return (isset($properties['namespace'])
            ? $properties['namespace']
            : studly_case($properties['slug'])
        );
    }

}
