<?php

namespace Hierarchy\Alexa\Package\Providers;

use Illuminate\Support\ServiceProvider;

/**
 *
 */
class GeneratorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerMakeMigrationCommand();
        $this->registerMakepackageCommand();
        $this->registerPackageMakeApiCommand();
        $this->registerMakePackageControlCommand();
        $this->registerMakePackageModelCommand();
    }

    /**
     * Register the make:package command.
     *
     * @return void
     */
    private function registerMakeMigrationCommand()
    {
        $this->app->singleton('command.make.package.migration', function ($app) {
            return $app['Hierarchy\Alexa\Package\Console\Commands\MakeMigrationCommand'];
        });

        $this->commands('command.make.package.migration');
    }

    /**
     * Register the make:package command.
     *
     * @return void
     */
    private function registerMakepackageCommand()
    {
        $this->app->singleton('command.make.package', function ($app) {
            return $app['Hierarchy\Alexa\Package\Console\Commands\MakePackageCommand'];
        });

        $this->commands('command.make.package');
    }

    /**
     * [registerpackageMakeApiCommand description]
     * @return [type] [description]
     */
    public function registerPackageMakeApiCommand()
    {
        $this->app->singleton('command.make.package.api', function ($app) {
            return $app['Hierarchy\Alexa\Package\Console\Commands\PackageMakeApiCommand'];
        });

        $this->commands('command.make.package.api');
    }

    /**
     * [registerMakePackageControlCommand description]
     * @return [type] [description]
     */
    public function registerMakePackageControlCommand()
    {
        $this->app->singleton('command.make.contoller', function ($app) {
            return $app['Hierarchy\Alexa\Package\Console\Commands\MakePackageControlCommand'];
        });

        $this->commands('command.make.contoller');
    }

    /**
     * [registerMakePackageModelCommand description]
     * @return [type] [description]
     */
    public function registerMakePackageModelCommand()
    {
        $this->app->singleton('command.make.model', function ($app) {
            return $app['Hierarchy\Alexa\Package\Console\Commands\MakeModelCommand'];
        });

        $this->commands('command.make.model');
    }
}
