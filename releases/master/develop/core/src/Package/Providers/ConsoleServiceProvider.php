<?php

namespace Hierarchy\Alexa\Package\Providers;

use Illuminate\Support\ServiceProvider;

class ConsoleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerInstallCommand();
        $this->registerUpdateCommand();
        $this->registerCacheCommand();
        $this->registerWidgetCommand();
        $this->registerDisableCommand();
        $this->registerEnableCommand();
        $this->registerListCommand();
        $this->registerMigrateCommand();
        $this->registerMigrateRefreshCommand();
        $this->registerMigrateResetCommand();
        $this->registerMigrateRollbackCommand();
        $this->registerSeedCommand();
        $this->registerPackagePublish();
        $this->registerDBInstallCommand();
        $this->registerOptimizeCommand();
    }

    public function registerInstallCommand()
    {
        $this->app->singleton('command.package.install', function ($app) {
            return new \Hierarchy\Alexa\Package\Console\Commands\PackageInstallCommand($app['files'], $app['package']);
        });

        $this->commands('command.package.install');
    }
    /**
     * [registerUpdateCommand description]
     * @return [type] [description]
     */
    public function registerUpdateCommand()
    {
        $this->app->singleton('command.package.update', function ($app) {
            return new \Hierarchy\Alexa\Package\Console\Commands\UpdatePackageCommand($app['files'], $app['package']);
        });

        $this->commands('command.package.update');
    }

    public function registerWidgetCommand()
    {
        $this->app->singleton('command.package.widget', function ($app) {
            return new \Hierarchy\Alexa\Package\Console\Commands\MakeWidgetCommand($app['files'], $app['package']);
        });

        $this->commands('command.package.widget');
    }

    /**
     * Register the package:disable command.
     *
     * @return void
     */
    protected function registerCacheCommand()
    {
        $this->app->singleton('command.package.cache', function () {
            return new \Hierarchy\Alexa\Package\Console\Commands\PackageCacheCommand;
        });

        $this->commands('command.package.cache');
    }

    /**
     * Register the package:disable command.
     *
     * @return void
     */
    protected function registerDisableCommand()
    {
        $this->app->singleton('command.package.disable', function () {
            return new \Hierarchy\Alexa\Package\Console\Commands\PackageDisableCommand;
        });

        $this->commands('command.package.disable');
    }

    /**
     * Register the package:enable command.
     *
     * @return void
     */
    protected function registerEnableCommand()
    {
        $this->app->singleton('command.package.enable', function () {
            return new \Hierarchy\Alexa\Package\Console\Commands\PackageEnableCommand;
        });

        $this->commands('command.package.enable');
    }

    /**
     * Register the package:list command.
     *
     * @return void
     */
    protected function registerListCommand()
    {
        $this->app->singleton('command.package.list', function ($app) {
            return new \Hierarchy\Alexa\Package\Console\Commands\PackageListCommand($app['package']);
        });

        $this->commands('command.package.list');
    }

    /**
     * Register the package:migrate command.
     *
     * @return void
     */
    protected function registerMigrateCommand()
    {
        $this->app->singleton('command.package.migrate', function ($app) {
            return new \Hierarchy\Alexa\Package\Console\Commands\PackageMigrateCommand($app['migrator'], $app['package']);
        });

        $this->commands('command.package.migrate');
    }

    /**
     * Register the package:migrate:refresh command.
     *
     * @return void
     */
    protected function registerMigrateRefreshCommand()
    {
        $this->app->singleton('command.package.migrate.refresh', function () {
            return new \Hierarchy\Alexa\Package\Console\Commands\PackageMigrateRefreshCommand;
        });

        $this->commands('command.package.migrate.refresh');
    }

    /**
     * Register the package:migrate:reset command.
     *
     * @return void
     */
    protected function registerMigrateResetCommand()
    {
        $this->app->singleton('command.package.migrate.reset', function ($app) {
            return new \Hierarchy\Alexa\Package\Console\Commands\PackageMigrateResetCommand($app['package'], $app['files'], $app['migrator']);
        });

        $this->commands('command.package.migrate.reset');
    }

    /**
     * Register the package:migrate:rollback command.
     *
     * @return void
     */
    protected function registerMigrateRollbackCommand()
    {
        $this->app->singleton('command.package.migrate.rollback', function ($app) {
            return new \Hierarchy\Alexa\Package\Console\Commands\PackageMigrateRollbackCommand($app['package']);
        });

        $this->commands('command.package.migrate.rollback');
    }

    /**
     * Register the package:seed command.
     *
     * @return void
     */
    protected function registerSeedCommand()
    {
        $this->app->singleton('command.package.seed', function ($app) {
            return new \Hierarchy\Alexa\Package\Console\Commands\PackageSeedCommand($app['package']);
        });

        $this->commands('command.package.seed');
    }

    /**
     * [registerPackagePublish description]
     * @return [type] [description]
     */
    public function registerPackagePublish()
    {
        $this->app->singleton('command.package.publish', function ($app) {
            return new \Hierarchy\Alexa\Package\Console\Commands\PackagePublishCommand($app['files']);
        });

        $this->commands('command.package.publish');
    }

    public function registerDBInstallCommand()
    {
        $this->app->singleton('command.package.dbinstall', function ($app) {
            return new \Hierarchy\Alexa\Package\Console\Commands\DBInstallCommand($app['package']);
        });

        $this->commands('command.package.dbinstall');
    }

    /**
     * Register the module:optimize command.
     *
     * @return void
     */
    protected function registerOptimizeCommand()
    {
        $this->app->singleton('command.package.optimize', function () {
            return new \Hierarchy\Alexa\Package\Console\Commands\PackageOptimizeCommand;
        });
        $this->commands('command.package.optimize');
    }

}
