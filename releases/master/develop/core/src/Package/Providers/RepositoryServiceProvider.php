<?php

namespace Hierarchy\Alexa\Package\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $driver = ucfirst(config('package.driver'));

        if ($driver == 'Custom') {
            $namespace = config('package.custom_driver');
        } else {
            $namespace = 'Hierarchy\Alexa\Package\Repositories\\' . $driver . 'Repository';
        }

        $this->app->bind('Hierarchy\Alexa\Package\Contracts\RepositoryInterface', $namespace);
    }
}
