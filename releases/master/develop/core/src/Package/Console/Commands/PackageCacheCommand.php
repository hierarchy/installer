<?php

namespace Hierarchy\Alexa\Package\Console\Commands;

use Illuminate\Console\Command;

class PackageCacheCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'package:cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset cached instance of enabled and disabled package';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        // $this->laravel['package']->cache();
        // $this->info("Package were successfully cached.");
    }
}
