<?php

namespace Hierarchy\Alexa\Package\Console\Commands;

use Config;
use Hierarchy\Alexa\Package\Package;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class PackageSeedCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'package:seed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed the database with records for a specific or all package';

    /**
     * @var package
     */
    protected $package;

    /**
     * Create a new command instance.
     *
     * @param package  $package
     */
    public function __construct(Package $package)
    {
        parent::__construct();

        $this->package = $package;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $slug = $this->argument('slug');

        if (isset($slug)) {
            if (!$this->package->exists($slug)) {
                return $this->error("Module does not exist.");
            }

            // set connection package
            $path = $this->package->getPackagePath($slug);
            try {
                $dotenv = new \Dotenv\Dotenv($path . 'Current');
                $dotenv->overload();
                $dbConnection = env('DB_CONNECTION');

                $conn = Config::get("database.connections.{$dbConnection}");
                $conn['host'] = env('DB_HOST');
                $conn['database'] = env('DB_DATABASE');
                $conn['username'] = env('DB_USERNAME');
                $conn['password'] = env('DB_PASSWORD');

                $defaultConnections = config('database.connections');
                $addConnections = array_merge($defaultConnections, [$slug => $conn]);
                Config::set('database.connections', $addConnections);
            } catch (\Exception $e) {
            }

            if ($this->package->isEnabled($slug)) {
                $this->seed($slug);
            } elseif ($this->option('force')) {
                $this->seed($slug);
            }

            return;
        } else {
            if ($this->option('force')) {
                $packages = $this->package->all();
            } else {
                $packages = $this->package->enabled();
            }

            foreach ($packages as $package) {
                $this->seed($package['slug']);
            }
        }
    }

    /**
     * Seed the specific module.
     *
     * @param  string $package
     * @return array
     */
    protected function seed($slug)
    {
        $package = $this->package->where('slug', $slug)->first();
        $params = [];
        $namespacePath = $this->package->getNamespace();
        $rootSeeder = $package['namespace'] . 'DatabaseSeeder';
        $fullPath = $namespacePath . '\\' . $package['namespace'] . '\Current\Database\Seeds\\' . $rootSeeder;

        if (class_exists($fullPath)) {
            if ($this->option('class')) {
                $params['--class'] = $this->option('class');
            } else {
                $params['--class'] = $fullPath;
            }

            if ($option = $this->option('database')) {
                $params['--database'] = $option;
            }

            if ($option = $this->option('force')) {
                $params['--force'] = $option;
            }

            $this->call('db:seed', $params);
        }
        $this->info('success seeding');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [['slug', InputArgument::OPTIONAL, 'package slug.']];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['class', null, InputOption::VALUE_OPTIONAL, 'The class name of the module\'s root seeder.'],
            ['database', null, InputOption::VALUE_OPTIONAL, 'The database connection to seed.'],
            ['force', null, InputOption::VALUE_NONE, 'Force the operation to run while in production.'],
        ];
    }
}
