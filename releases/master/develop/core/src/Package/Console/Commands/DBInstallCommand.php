<?php

namespace Hierarchy\Alexa\Package\Console\Commands;

use Config;
use Hierarchy\Alexa\Package\Package;
use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class DBInstallCommand extends Command
{
    use ConfirmableTrait;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'package:db:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset and re-run all migrations for a specific or all packages';

    /**
     * Create a new command instance.
     *
     * @param package  $package
     */
    public function __construct(Package $package)
    {
        parent::__construct();

        $this->package = $package;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        if (!$this->confirmToProceed()) {
            return null;
        }

        $slug = $this->argument('slug');
        $database = $this->option('database');

        // set connection package
        $path = $this->package->getPackagePath($slug);
        try {
            $dotenv = new \Dotenv\Dotenv($path . 'Current');
            $dotenv->overload();
            $dbConnection = env('DB_CONNECTION');

            $conn = Config::get("database.connections.{$dbConnection}");
            $conn['host'] = env('DB_HOST');
            $conn['database'] = env('DB_DATABASE');
            $conn['username'] = env('DB_USERNAME');
            $conn['password'] = env('DB_PASSWORD');

            $defaultConnections = config('database.connections');
            $addConnections = array_merge($defaultConnections, [$slug => $conn]);
            Config::set('database.connections', $addConnections);
            // set database connection if exist env
            $database = (null !== $this->option('database')) ?: $slug;
        } catch (\Exception $e) {
        }

        // $this->call('package:migrate:reset', [
        //     'slug' => $slug,
        //     '--database' => $database,
        //     '--force' => $this->option('force'),
        //     '--pretend' => $this->option('pretend'),
        // ]);

        $this->call('package:migrate', [
            'slug' => $slug,
            '--database' => $database,
        ]);

        if ($this->needsSeeding()) {
            $this->runSeeder($slug, $database);
        }

        if (isset($slug)) {
            $this->info("package has been refreshed.");
        } else {
            $this->info("All packages have been refreshed.");
        }
    }

    /**
     * Determine if the developer has requested database seeding.
     *
     * @return bool
     */
    protected function needsSeeding()
    {
        return true;
    }

    /**
     * Run the package seeder command.
     *
     * @param  string $database
     * @return void
     */
    protected function runSeeder($slug = null, $database = null)
    {
        $this->call('package:seed', [
            'slug' => $slug,
            '--database' => $database,
        ]);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [['slug', InputArgument::REQUIRED, 'package slug.']];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['database', null, InputOption::VALUE_OPTIONAL, 'The database connection to use.'],
            ['force', null, InputOption::VALUE_NONE, 'Force the operation to run while in production.'],
            ['pretend', null, InputOption::VALUE_NONE, 'Dump the SQL queries that would be run.'],
            ['seed', null, InputOption::VALUE_NONE, 'Indicates if the seed task should be re-run.'],
        ];
    }
}
