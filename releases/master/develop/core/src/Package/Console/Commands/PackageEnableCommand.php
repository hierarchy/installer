<?php

namespace Hierarchy\Alexa\Package\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class PackageEnableCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'package:enable';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Enable a package';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $slug = $this->argument('slug');

        if ($this->laravel['package']->isDisabled($slug)) {
            $this->laravel['package']->enable($slug);

            $this->info("Package was enabled successfully.");
        } else {
            $this->comment("Package is already enabled.");
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['slug', InputArgument::REQUIRED, 'Package slug.'],
        ];
    }
}
