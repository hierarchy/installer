<?php

namespace Hierarchy\Alexa\Package\Console\Commands;

use Hierarchy\Alexa\Package\Package;
use Illuminate\Console\Command;

class PackageListCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'package:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List all application packages';

    /**
     * @var packages
     */
    protected $package;

    /**
     * The table headers for the command.
     *
     * @var array
     */
    protected $headers = ['#', 'Name', 'Slug', 'Description', 'Status'];

    /**
     * Create a new command instance.
     *
     * @param packages  $package
     */
    public function __construct(Package $package)
    {
        parent::__construct();

        $this->package = $package;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $packages = $this->package->all();

        if (count($packages) == 0) {
            return $this->error("Your application doesn't have any packages.");
        }

        $this->displayPackages($this->getPackages());
    }

    /**
     * Get all packages.
     *
     * @return array
     */
    protected function getPackages()
    {
        $packages = $this->package->all();
        $results = [];

        foreach ($packages as $package) {
            $results[] = $this->getPackageInformation($package);
        }

        return array_filter($results);
    }

    /**
     * Returns package manifest information.
     *
     * @param string  $package
     * @return array
     */
    protected function getPackageInformation($package)
    {
        return [
            '#' => $package['order'],
            'name' => $package['name'],
            'slug' => $package['slug'],
            'description' => $package['description'],
            'status' => ($this->package->isEnabled($package['slug'])) ? 'Enabled' : 'Disabled',
        ];
    }

    /**
     * Display the package information on the console.
     *
     * @param array  $packages
     * @return void
     */
    protected function displayPackages(array $packages)
    {
        $this->table($this->headers, $packages);
    }
}
