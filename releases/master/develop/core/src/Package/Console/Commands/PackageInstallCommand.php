<?php

namespace Hierarchy\Alexa\Package\Console\Commands;

use GitWrapper\GitWrapper;
use Hierarchy\Alexa\Package\Package;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Process\Process;

class PackageInstallCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'package:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'install a package';

    /**
     * Array to store the configuration details.
     *
     * @var array
     */
    protected $container;

    /**
     * [$wrapper description]
     * @var [type]
     */
    protected $wrapper;

    /**
     * [$updated description]
     * @var [type]
     */
    protected $updated;

    /**
     * Create a new command instance.
     *
     * @param Filesystem  $files
     * @param Package  $package
     */
    public function __construct(Filesystem $files, Package $package)
    {
        parent::__construct();

        $this->files = $files;
        $this->package = $package;
        $this->wrapper = new GitWrapper();
        $this->updated = date('YmdHis');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->container['slug'] = strtolower($this->argument('slug'));
        $this->container['git_url'] = $this->argument('git_url');

        if ($this->package->exists($this->container['slug'])) {
            $params = [];
            $params['slug'] = $this->container['slug'];
            $params['git_url'] = $this->container['git_url'];

            $this->call('package:update', $params);
            return;
        }

        $this->installPackageProsess();
    }

    public function installPackageProsess()
    {
        $steps = [
            'Generating folders...' => 'generateFolders',
            'Cloning source files...' => 'cloneSources',
        ];

        $progress = new ProgressBar($this->output, count($steps));
        $progress->start();

        foreach ($steps as $message => $function) {
            $progress->setMessage($message);

            $this->$function();

            $progress->advance();
        }

        $this->callSilent('package:cache');

        $progress->finish();
    }

    public function generateFolders()
    {
        if (!$this->files->isDirectory($this->package->getPath())) {
            $this->files->makeDirectory($this->package->getPath());
        }

        $this->files->makeDirectory($this->getPackagePath($this->container['slug'], true));
        $this->files->makeDirectory($this->getPackagePath($this->container['slug']) . 'Releases');
        $this->files->put($this->getPackagePath($this->container['slug']) . 'Releases/.gitkeep', '');
    }

    public function cloneSources()
    {
        // path working update
        $pathCopy = $this->package->getPackagePath($this->container['slug']) . 'Releases/' . $this->updated;
        $packagePath = $this->package->getPackagePath($this->container['slug']);

        // Optionally specify a private key other than one of the defaults.
        // $this->wrapper->setPrivateKey(base_path('/') . '../');

        // Clone a repo into `/path/to/working/copy`, get a working copy object.
        $git = $this->wrapper->clone($this->container['git_url'], $pathCopy);

        // git print output
        $this->info($git->getOutput());

        $processgui = new Process("ln -nfs {$pathCopy} {$packagePath}/Current");
        $processgui->setTimeout(3600);
        $processgui->setIdleTimeout(300);
        $processgui->run();
    }

    /**
     * Get the path to the package.
     *
     * @param  string $slug
     * @return string
     */
    protected function getPackagePath($slug = null, $allowNotExists = false)
    {
        if ($slug) {
            return $this->package->getPackagePath($slug, $allowNotExists);
        }

        return $this->package->getPath();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['slug', InputArgument::REQUIRED, 'The slug of the package'],
            ['git_url', InputArgument::REQUIRED, 'Git Url for new updated package'],
        ];
    }
}
