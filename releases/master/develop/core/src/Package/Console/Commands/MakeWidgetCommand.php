<?php

namespace Hierarchy\Alexa\Package\Console\Commands;

use Hierarchy\Alexa\Package\Package;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputArgument;

class MakeWidgetCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'package:make:widget';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new package widget file';

    /**
     * Array to store the configuration details.
     *
     * @var array
     */
    protected $container;

    /**
     * Create a new command instance.
     *
     * @param Filesystem  $files
     * @param Package  $package
     */
    public function __construct(Filesystem $files, Package $package)
    {
        parent::__construct();

        $this->files = $files;
        $this->package = $package;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $name = explode('/', $this->argument('name'));
        $nameClass = (count($name) > 1) ? $name[1] : $this->argument('name');
        $this->container['slug'] = strtolower($this->argument('slug'));
        $this->container['name'] = strtolower($nameClass);
        $this->container['namespace'] = studly_case($this->container['slug']);

        if ($this->package->exists($this->container['slug'])) {
            $this->makeFile();

            $this->info('Create package widget: ' . $this->getFilename());

            return exec('composer dump-autoload');
        }

        return $this->info('package does not exist.');
    }

    /**
     * Create a new migration file.
     *
     * @return int
     */
    protected function makeFile()
    {
        if (!$this->files->isDirectory($this->package->getPackagePath($this->container['slug'] . '/Current/Widgets'))) {
            $this->files->makeDirectory($this->package->getPackagePath($this->container['slug'] . '/Current/Widgets'));
        }
        $name = explode('/', $this->argument('name'));
        if (count($name) > 1 && !$this->files->isDirectory($this->package->getPackagePath($this->container['slug'] . '/Current/Widgets/' . studly_case($name[0])))) {
            $this->files->makeDirectory($this->package->getPackagePath($this->container['slug'] . '/Current/Widgets/' . studly_case($name[0])));
        }
        return $this->files->put($this->getDestinationFile(), $this->getStubContent());
    }

    /**
     * Get file destination.
     *
     * @return string
     */
    protected function getDestinationFile()
    {
        return $this->getPath() . $this->formatContent($this->getFilename());
    }

    /**
     * Get package migration path.
     *
     * @return string
     */
    protected function getPath()
    {
        $name = explode('/', $this->argument('name'));
        $path = $this->package->getPackagePath($this->container['slug']);
        $sub = '';
        if (is_array($name) && count($name) > 1) {
            $sub = studly_case($name[0]) . '/';
        }
        return $path . 'Current/Widgets/' . $sub;
    }

    /**
     * Get the migration filename.
     *
     * @return string
     */
    protected function getFilename()
    {
        return studly_case($this->container['name']) . '.php';
    }

    /**
     * Get the stub content.
     *
     * @return string
     */
    protected function getStubContent()
    {
        return $this->formatContent($this->files->get(__DIR__ . '/../stubs/widget.stub'));
    }

    /**
     * Replace placeholder text with correct values.
     *
     * @return string
     */
    protected function formatContent($content)
    {
        $name = explode('/', $this->argument('name'));
        $sub = '';
        if (is_array($name) && count($name) > 1) {
            $sub = '\\' . studly_case($name[0]);
        }
        return str_replace(
            ['{{className}}', '{{namespace}}', '{{package}}', '{{view}}', '{{path}}', '{{sub}}'],
            [studly_case($this->container['name']), $this->container['namespace'], $this->container['slug'], $this->container['name'], $this->package->getNamespace(), $sub],
            $content
        );
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['slug', InputArgument::REQUIRED, 'The slug of the package'],
            ['name', InputArgument::REQUIRED, 'The name of the widget'],
        ];
    }
}
