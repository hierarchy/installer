<?php

namespace Hierarchy\Alexa\Package\Console\Commands;

use Hierarchy\Alexa\Package\Package;
use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;
use Illuminate\Database\Migrations\Migrator;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class PackageMigrateResetCommand extends Command
{
    use ConfirmableTrait;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'package:migrate:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rollback all database migrations for a specific or all packages';

    /**
     * @var packages
     */
    protected $package;

    /**
     * @var Migrator
     */
    protected $migrator;

    /**
     * @var Filesystem
     */
    protected $files;

    /**
     * Create a new command instance.
     *
     * @param packages  $package
     * @param Filesystem  $files
     * @param Migrator  $migrator
     */
    public function __construct(Package $package, Filesystem $files, Migrator $migrator)
    {
        parent::__construct();

        $this->package = $package;
        $this->files = $files;
        $this->migrator = $migrator;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        if (!$this->confirmToProceed()) {
            return null;
        }

        $slug = $this->argument('slug');

        if (!empty($slug)) {
            if ($this->package->isEnabled($slug)) {
                return $this->reset($slug);
            } elseif ($this->option('force')) {
                return $this->reset($slug);
            }
        } else {
            if ($this->option('force')) {
                $packages = $this->package->all()->reverse();
            } else {
                $packages = $this->package->enabled()->reverse();
            }

            foreach ($packages as $package) {
                $this->reset($package['slug']);
            }
        }
    }

    /**
     * Run the migration reset for the specified package.
     *
     * Migrations should be reset in the reverse order that they were
     * migrated up as. This ensures the database is properly reversed
     * without conflict.
     *
     * @param  string $slug
     * @return mixed
     */
    protected function reset($slug)
    {
        $this->migrator->setconnection($this->input->getOption('database'));

        $pretend = $this->input->getOption('pretend');
        $migrationPath = $this->getMigrationPath($slug);
        $migrations = array_reverse($this->migrator->getMigrationFiles($migrationPath));

        if (count($migrations) == 0) {
            return $this->error('Nothing to rollback.');
        }

        foreach ($migrations as $migration) {
            $this->info('Migration: ' . $migration);
            $this->runDown($slug, $migration, $pretend);
        }
    }

    /**
     * Run "down" a migration instance.
     *
     * @param  string $slug
     * @param  object $migration
     * @param  bool   $pretend
     * @return void
     */
    protected function runDown($slug, $migration, $pretend)
    {
        $migrationPath = $this->getMigrationPath($slug);
        $file = (string) $migrationPath . '/' . $migration . '.php';
        $classFile = implode('_', array_slice(explode('_', basename($file, '.php')), 4));
        $class = studly_case($classFile);
        $table = $this->laravel['config']['database.migrations'];

        include $file;

        $instance = new $class;
        $instance->down();

        $this->laravel['db']->table($table)
            ->where('migration', $migration)
            ->delete();
    }

    /**
     * Get the console command parameters.
     *
     * @param  string $slug
     * @return array
     */
    protected function getParameters($slug)
    {
        $params = [];

        $params['--path'] = $this->getMigrationPath($slug);

        if ($option = $this->option('database')) {
            $params['--database'] = $option;
        }

        if ($option = $this->option('pretend')) {
            $params['--pretend'] = $option;
        }

        if ($option = $this->option('seed')) {
            $params['--seed'] = $option;
        }

        return $params;
    }

    /**
     * Get migrations path.
     *
     * @return string
     */
    protected function getMigrationPath($slug)
    {
        $path = $this->package->getPackagePath($slug) . 'Current/Database/Migrations';

        return $path;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [['slug', InputArgument::OPTIONAL, 'package slug.']];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['database', null, InputOption::VALUE_OPTIONAL, 'The database connection to use.'],
            ['force', null, InputOption::VALUE_NONE, 'Force the operation to run while in production.'],
            ['pretend', null, InputOption::VALUE_OPTIONAL, 'Dump the SQL queries that would be run.'],
            ['seed', null, InputOption::VALUE_OPTIONAL, 'Indicates if the seed task should be re-run.'],
        ];
    }
}
