<?php

namespace Hierarchy\Alexa\Package\Console\Commands;

use App;
use Config;
use Hierarchy\Alexa\Package\Package;
use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;
use Illuminate\Database\Migrations\Migrator;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class PackageMigrateCommand extends Command
{
    use ConfirmableTrait;
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'package:migrate';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run the database migrations for a specific or all packages';
    /**
     * @var packages
     */
    protected $package;
    /**
     * @var Migrator
     */
    protected $migrator;
    /**
     * Create a new command instance.
     *
     * @param Migrator  $migrator
     * @param packages  $package
     */
    public function __construct(Migrator $migrator, Package $package)
    {
        parent::__construct();
        $this->migrator = $migrator;
        $this->package = $package;
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->prepareDatabase();
        if (!empty($this->argument('slug'))) {
            $package = $this->package->where('slug', $this->argument('slug'))->first();

            $this->loadConfigDB($package['slug']);

            if ($this->package->isEnabled($package['slug'])) {
                return $this->migrate($package['slug']);
            } elseif ($this->option('force')) {
                return $this->migrate($package['slug']);
            } else {
                return $this->error('Nothing to migrate.');
            }
        } else {
            if ($this->option('force')) {
                $packages = $this->package->all();
            } else {
                $packages = $this->package->enabled();
            }
            foreach ($packages as $package) {
                $this->migrate($package['slug']);
            }
        }
    }

    protected function loadConfigDB($slug)
    {
        $path = $this->package->getPackagePath($slug);
        try {
            $dotenv = new \Dotenv\Dotenv($path . 'Current');
            $dotenv->overload();
            $dbConnection = env('DB_CONNECTION');
            dd($dotenv);
            $conn = Config::get("database.connections.{$dbConnection}");
            $conn['host'] = env('DB_HOST');
            $conn['database'] = env('DB_DATABASE');
            $conn['username'] = env('DB_USERNAME');
            $conn['password'] = env('DB_PASSWORD');

            $defaultConnections = config('database.connections');
            $addConnections = array_merge($defaultConnections, [$slug => $conn]);
            Config::set('database.connections', $addConnections);
        } catch (\Exception $e) {
        }
    }

    /**
     * Run migrations for the specified package.
     *
     * @param  string $slug
     * @return mixed
     */
    protected function migrate($slug)
    {
        if ($this->package->exists($slug)) {
            $pretend = Arr::get($this->option(), 'pretend', false);
            $path = $this->getMigrationPath($slug);
            if (floatval(App::version()) > 5.1) {
                $pretend = ['pretend' => $pretend];
            }
            $this->migrator->run($path, $pretend);
            // Once the migrator has run we will grab the note output and send it out to
            // the console screen, since the migrator itself functions without having
            // any instances of the OutputInterface contract passed into the class.
            foreach ($this->migrator->getNotes() as $note) {
                if (!$this->option('quiet')) {
                    $this->line($note);
                }
            }
            // Finally, if the "seed" option has been given, we will re-run the database
            // seed task to re-populate the database, which is convenient when adding
            // a migration and a seed at the same time, as it is only this command.
            if ($this->option('seed')) {
                $this->call('package:seed', ['package' => $slug, '--force' => true]);
            }
        } else {
            return $this->error("package does not exist.");
        }
    }
    /**
     * Get migration directory path.
     *
     * @param  string $slug
     * @return string
     */
    protected function getMigrationPath($slug)
    {
        $path = $this->package->getpackagePath($slug);
        return $path . 'Current/Database/Migrations/';
    }
    /**
     * Prepare the migration database for running.
     *
     * @return void
     */
    protected function prepareDatabase()
    {
        $this->migrator->setConnection($this->option('database'));
        if (!$this->migrator->repositoryExists()) {
            $options = array('--database' => $this->option('database'));
            $this->call('migrate:install', $options);
        }
    }
    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [['slug', InputArgument::OPTIONAL, 'package slug.']];
    }
    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['database', null, InputOption::VALUE_OPTIONAL, 'The database connection to use.'],
            ['force', null, InputOption::VALUE_NONE, 'Force the operation to run while in production.'],
            ['pretend', null, InputOption::VALUE_NONE, 'Dump the SQL queries that would be run.'],
            ['seed', null, InputOption::VALUE_NONE, 'Indicates if the seed task should be re-run.'],
        ];
    }
}
