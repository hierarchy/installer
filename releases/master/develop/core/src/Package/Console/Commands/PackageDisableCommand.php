<?php

namespace Hierarchy\Alexa\Package\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class PackageDisableCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'package:disable';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Disable a package';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $slug = $this->argument('slug');

        if ($this->laravel['package']->isEnabled($slug)) {
            $this->laravel['package']->disable($slug);

            $this->info("Package was disabled successfully.");
        } else {
            $this->comment("Package is already disabled.");
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['slug', InputArgument::REQUIRED, 'Package slug.'],
        ];
    }
}
