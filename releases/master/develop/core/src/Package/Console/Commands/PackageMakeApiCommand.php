<?php

namespace Hierarchy\Alexa\Package\Console\Commands;

use Hierarchy\Alexa\Package\Package;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;

class PackageMakeApiCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'package:make:api';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Api Package and bootstrap it';

    /**
     * package folders to be created.
     *
     * @var array
     */
    protected $packageFolders = [
        'Current/Api/',
        'Current/Api/V{{version}}',
        'Current/Api/V{{version}}/{{namespace}}',
        'Current/Entities/',
    ];

    /**
     * package files to be created.
     *
     * @var array
     */
    protected $packageFiles = [
        'Current/Api/V{{version}}/{{namespace}}/{{namespace}}Control.php',
        'Current/Api/V{{version}}/{{namespace}}/{{namespace}}Transformer.php',
        'Current/Entities/{{namespace}}.php',
    ];

    /**
     * package stubs used to populate defined files.
     *
     * @var array
     */
    protected $Packagetubs = [
        'apicontroller.stub',
        'apitransformer.stub',
        'apimodel.stub',
    ];

    /**
     * The package instance.
     *
     * @var package
     */
    protected $package;

    /**
     * The filesystem instance.
     *
     * @var Filesystem
     */
    protected $files;

    /**
     * Array to store the configuration details.
     *
     * @var array
     */
    protected $container;

    /**
     * Create a new command instance.
     *
     * @param Filesystem  $files
     * @param package  $package
     */
    public function __construct(Filesystem $files, Package $package)
    {
        parent::__construct();

        $this->files = $files;
        $this->package = $package;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->container['slug'] = strtolower($this->argument('slug'));
        $this->container['name'] = strtolower($this->argument('name'));
        $this->container['namespace'] = Str::studly($this->container['name']);

        $this->displayHeader('make_api_introduction');

        if (!$this->package->exists($this->container['slug'])) {
            $this->comment('package [' . $this->container['slug'] . '] does not exist.');
            return true;
        } else {

            $this->stepOne();
        }
    }

    /**
     * Step 1: Configure package manifest.
     *
     * @return mixed
     */
    private function stepOne()
    {
        $this->comment('Your create api on package : ' . $this->container['slug']);

        $this->container['slug'] = $this->ask('Please enter the package: ', $this->container['slug']);
        $this->container['name'] = $this->ask('Please enter the name of the api package:', $this->container['name']);
        $this->container['namespace'] = $this->ask('Please enter the namespace for the package:', $this->container['namespace']);
        $this->container['version'] = $this->ask('Please enter the package version:', '1');
        $this->container['description'] = $this->ask('Please enter the description of the package:', 'This is the description for the ' . $this->container['name'] . ' package.');
        $this->container['author'] = $this->ask('Please enter the author of the package:', ' ');
        $this->container['license'] = $this->ask('Please enter the package license:', 'MIT');

        $this->comment('You have provided the following manifest information:');
        $this->comment('Package:     ' . $this->container['slug']);
        $this->comment('Api Name:    ' . $this->container['name']);
        $this->comment('Namespace:   ' . $this->container['namespace']);
        $this->comment('Version:     ' . $this->container['version']);
        $this->comment('Description: ' . $this->container['description']);
        $this->comment('Author:      ' . $this->container['author']);
        $this->comment('License:     ' . $this->container['license']);

        if ($this->confirm('Do you wish to continue?')) {
            $this->comment('Thanks! That\'s all we need.');
            $this->comment('Now relax while your api is generated for you.');

            $this->generate();

        } else {
            return $this->stepOne();
        }

        return true;
    }

    /**
     * Generate the package.
     */
    protected function generate()
    {
        $steps = [
            'Generating folders...' => 'generateFolders',
            'Generating .gitkeep...' => 'generateGitkeep',
            'Generating files...' => 'generateFiles',
            // 'Resetting package cache...'  => 'resetCache',
            // 'Optimizing Laravel...'     => 'optimizeLaravel'
        ];

        $progress = new ProgressBar($this->output, count($steps));
        $progress->start();

        foreach ($steps as $message => $function) {
            $progress->setMessage($message);

            $this->$function();

            $progress->advance();
        }

        $progress->finish();

        $this->info("\nAdd this route");
        $this->error("{$this->formatContent($this->files->get(__DIR__ . '/../stubs/apiroute.stub'))}");
        $this->info("\nin routes.php .");

        $this->info("\npackage generated successfully.");

    }

    /**
     * Generate defined package folders.
     *
     * @return void
     */
    protected function generateFolders()
    {
        foreach ($this->packageFolders as $folder) {
            if (!$this->files->isDirectory($this->getPackagePath(Str::studly($this->container['slug'])) . '/' . $this->formatContent($folder))) {
                $this->files->makeDirectory($this->getPackagePath(Str::studly($this->container['slug'])) . '/' . $this->formatContent($folder));
            }
        }
    }

    /**
     * Generate defined package files.
     *
     * @return void
     */
    protected function generateFiles()
    {
        foreach ($this->packageFiles as $key => $file) {
            $file = $this->formatContent($file);

            $this->files->put($this->getDestinationFile($file), $this->getStubContent($key));
        }
    }

    /**
     * Generate .gitkeep files within generated folders.
     *
     * @return null
     */
    protected function generateGitkeep()
    {
        $packagePath = $this->getPackagePath($this->container['slug']);
        foreach ($this->packageFolders as $folder) {
            $gitkeep = $packagePath . $this->formatContent($folder) . '/.gitkeep';
            $this->files->put($gitkeep, '');
        }
    }

    /**
     * Reset package cache of enabled and disabled package.
     *
     * @return void
     */
    protected function resetCache()
    {
        return $this->callSilent('package:cache');
    }

    /**
     * Optimize Laravel for better performance.
     *
     * @return void
     */
    protected function optimizeLaravel()
    {
        return $this->callSilent('optimize');
    }

    /**
     * Get the path to the package.
     *
     * @param  string $slug
     * @return string
     */
    protected function getPackagePath($slug = null, $allowNotExists = false)
    {
        if ($slug) {
            return $this->package->getPackagePath($slug, $allowNotExists);
        }

        return $this->package->getPath();
    }

    /**
     * Get destination file.
     *
     * @param  string $file
     * @return string
     */
    protected function getDestinationFile($file)
    {
        return $this->getPackagePath($this->container['slug']) . $this->formatContent($file);
    }

    /**
     * Get stub content by key.
     *
     * @param int  $key
     * @return string
     */
    protected function getStubContent($key)
    {
        return $this->formatContent($this->files->get(__DIR__ . '/../stubs/' . $this->Packagetubs[$key]));
    }

    /**
     * Replace placeholder text with correct values.
     *
     * @return string
     */
    protected function formatContent($content)
    {
        return str_replace(
            ['{{slug}}', '{{packagenamespace}}', '{{name}}', '{{namespace}}', '{{version}}', '{{description}}', '{{author}}', '{{path}}', '{{pluralname}}'],
            [$this->container['slug'], Str::studly($this->container['slug']), $this->container['name'], $this->container['namespace'], $this->container['version'], $this->container['description'], $this->container['author'], $this->package->getNamespace(), Str::plural($this->container['name'])],
            $content
        );
    }

    /**
     * Pull the given stub file contents and display them on screen.
     *
     * @param string  $file
     * @param string  $level
     * @return mixed
     */
    protected function displayHeader($file = '', $level = 'info')
    {
        $stub = $this->files->get(__DIR__ . '/../stubs/console/' . $file . '.stub');
        return $this->$level($stub);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['slug', InputArgument::REQUIRED, 'The slug of the package'],
            ['name', InputArgument::REQUIRED, 'The name api in the package'],
        ];
    }
}
