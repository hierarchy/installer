<?php

namespace Hierarchy\Alexa\Package\Middleware;

use Closure;
use Config;

class HierarchyEnv
{

    protected $package;

    public function __construct()
    {
        $this->package = app('package');
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $slug)
    {
        if ($this->package->exists($slug)) {
            $path = $this->package->getPackagePath($slug);
            try {
                $dotenv = new \Dotenv\Dotenv($path . 'Current');
                $dotenv->overload();
                $dbConnection = env('DB_CONNECTION');

                $conn = config("database.connections.{$dbConnection}");
                $conn['host'] = env('DB_HOST');
                $conn['database'] = env('DB_DATABASE');
                $conn['username'] = env('DB_USERNAME');
                $conn['password'] = env('DB_PASSWORD');

                $defaultConnections = config('database.connections');
                $addConnections = array_merge($defaultConnections, [$slug => $conn]);
                Config::set('database.connections', $addConnections);

                // Config::set("database.default", $dbConnection);
                // Config::set("database.connections.{$dbConnection}.host", env('DB_HOST'));
                // Config::set("database.connections.{$dbConnection}.database", env('DB_DATABASE'));
                // Config::set("database.connections.{$dbConnection}.username", env('DB_USERNAME'));
                // Config::set("database.connections.{$dbConnection}.password", env('DB_PASSWORD'));
            } catch (\Exception $e) {
                return $next($request);
            }
        }

        return $next($request);
    }
}
