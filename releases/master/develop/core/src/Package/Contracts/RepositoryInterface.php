<?php

namespace Hierarchy\Alexa\Package\Contracts;

/**
 * Contracts Repository
 */
interface RepositoryInterface
{
    /**
     * Get all package manifest properties and store
     * in the respective container.
     *
     * @return bool
     */
    public function optimize();

    /**
     * Get all packages.
     *
     * @return Collection
     */
    public function all();

    /**
     * Get all package slugs.
     *
     * @return Collection
     */
    public function slugs();

    /**
     * Get packages based on where clause.
     *
     * @param  string  $key
     * @param  mixed   $value
     * @return Collection
     */
    public function where($key, $value);

    /**
     * Sort packages by given key in ascending order.
     *
     * @param  string  $key
     * @return Collection
     */
    public function sortBy($key);

    /**
     * Sort packages by given key in ascending order.
     *
     * @param  string  $key
     * @return Collection
     */
    public function sortByDesc($key);

    /**
     * Determines if the given package exists.
     *
     * @param  string  $slug
     * @return bool
     */
    public function exists($slug);

    /**
     * Returns a count of all packages.
     *
     * @return int
     */
    public function count();

    /**
     * Returns the packages defined manifest properties.
     *
     * @param string $slug
     * @return Collection
     */
    public function getManifest($slug);

    /**
     * Returns the given package property.
     *
     * @param  string       $property
     * @param  mixed|null   $default
     * @return mixed|null
     */
    public function get($property, $default = null);

    /**
     * Set the given package property value.
     *
     * @param  string  $property
     * @param  mixed   $value
     * @return bool
     */
    public function set($property, $value);

    /**
     * Get all enabled packages.
     *
     * @return Collection
     */
    public function enabled();

    /**
     * Get all disabled packages.
     *
     * @return Collection
     */
    public function disabled();

    /**
     * Determines if the specified package is enabled.
     *
     * @param  string  $slug
     * @return bool
     */
    public function isEnabled($slug);

    /**
     * Determines if the specified package is disabled.
     *
     * @param  string  $slug
     * @return bool
     */
    public function isDisabled($slug);

    /**
     * Enables the specified package.
     *
     * @param  string  $slug
     * @return bool
     */
    public function enable($slug);

    /**
     * Disables the specified package.
     *
     * @param  string  $slug
     * @return bool
     */
    public function disable($slug);
}
