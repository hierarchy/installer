<?php

namespace Hierarchy\Alexa\Media;

use Illuminate\Support\Facades\Facade;

class YoutubeFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {return 'youtube';}
}
