<?php

namespace Hierarchy\Alexa\Media\Traits;

use App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Image;

trait Upload
{
    protected $path; // = config('hicms.upload.path');
    protected $errors;
    protected $filename; // = str_random(30);
    protected $rules = [];
    protected $info = [];
    protected $fieldName = 'file';
    protected $mimeImage = [
        'image/gif',
        'image/jpeg',
        'image/png',
        'application/x-shockwave-flash',
        'image/psd',
        'image/bmp',
        'image/tiff',
        'image/jp2',
        'application/octet-stream',
        'application/x-shockwave-flash',
        'image/iff',
        'image/vnd.wap.wbmp',
        'image/xbm',
        'image/vnd.microsoft.icon',
    ];

    public function uploadFile(Request $request)
    {
        $file = $request->file($this->fieldName);

        if (!empty($this->rules) && !$this->validator($request, $this->rules)) {
            return $this->errors;
        }

        try {
            if ($file->isValid()) {
                $this->info['filename'] = $this->filename . '.' . $file->getClientOriginalExtension();
                $this->info['filesize'] = $file->getClientSize();
                $this->info['mimetype'] = $file->getClientMimeType();
                $this->info['extension'] = $file->getClientOriginalExtension();
                $this->info['path'] = $this->getPath();
                $this->info['name'] = str_slug($file->getClientOriginalName(), ' ');

                if ($request->has('base64') && !empty($request->base64)) {
                    $img = Image::make($request->base64);
                    $img->save($this->getPath() . $this->filename . '.' . $file->getClientOriginalExtension());
                } else {
                    $file->move($this->getPath(), $this->filename . '.' . $file->getClientOriginalExtension());
                }

                if (in_array($file->getClientMimeType(), $this->mimeImage)) {
                    $this->info['resolution'] = $this->informationImage($this->info['path'] . $this->filename . '.' . $file->getClientOriginalExtension());
                }
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return $this->info;
    }

    public function videoParser($url, $source = 'youtube')
    {
        $youtube = app('youtube');

        if ($source == 'youtube') {
            $videoId = $youtube->parseVidFromURL($url);
            $info = $youtube->getVideoInfo($videoId);

            $interval = new \DateInterval($info->contentDetails->duration);

            $video['id'] = $info->id;
            $video['title'] = $info->snippet->title;
            $video['description'] = $info->snippet->description;

            $video['image'] = $this->filename . '.jpg';
            $video['embedHtml'] = $info->player->embedHtml;
            $video['duration'] = $interval->h * 3600 + $interval->i * 60 + $interval->s;

            $image = $info->snippet->thumbnails->high->url;

            if (!empty($image)) {
                $img = Image::make($info->snippet->thumbnails->high->url);
                $img->save($this->getPath() . $video['image']);
            }
        }

        return $video;
    }

    public function validator($request, $rules)
    {
        if (!count($rules) > 0) {
            return true;
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $this->errors = $validator->messages()->toArray();
            return false;
        }

        return true;
    }

    public function informationImage($path)
    {
        $image = Image::make($path);
        return [
            'height' => $image->height(),
            'width' => $image->width(),
        ];
    }

    public function excelToArray($file)
    {
        $excel = App::make('excel');
        if (is_file($file)) {
            return $excel->selectSheetsByIndex(0)->load($file)->toArray();
        }
        return null;
    }

    public function setRules($rules = [])
    {
        $this->rules = $rules;
    }

    public function getRules()
    {
        return $this->rules;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function setPath($path = null)
    {
        $this->path = $path;
    }

    public function setFilename($filename)
    {
        $this->filename = $filename;
    }

    public function setFieldName($fieldName)
    {
        $this->fieldName = $fieldName;
    }
}
