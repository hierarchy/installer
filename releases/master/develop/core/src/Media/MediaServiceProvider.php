<?php

namespace Hierarchy\Alexa\Media;

use Hierarchy\Alexa\Media\Media;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

class MediaServiceProvider extends ServiceProvider
{
    /**
     * [boot description]
     * @return [type] [description]
     */
    public function boot()
    {
        # code...
    }

    /**
     *
     */
    public function register()
    {
        $this->app->register(\Intervention\Image\ImageServiceProvider::class);

        $loader = AliasLoader::getInstance();
        $loader->alias('Image', \Intervention\Image\Facades\Image::class);

        // youtube grabber
        $this->app->singleton('youtube', function () {
            return $this->app->make(\Hierarchy\Alexa\Media\Youtube::class, [config('youtube.KEY')]);
        });
        $loader->alias('Youtube', \Hierarchy\Alexa\Media\YoutubeFacade::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array('youtube');
    }
}
