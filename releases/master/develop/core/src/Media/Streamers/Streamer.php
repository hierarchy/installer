<?php

namespace Hierarchy\Alexa\Media\Streamers;

use Illuminate\Database\Eloquent\Model;

class Streamer
{
    /**
     * @var Song|string
     */
    protected $song;

    /**
     * @var Path|string
     */
    public $path;

    /**
     * @var string
     */
    protected $contentType;

    /**
     * BaseStreamer constructor.
     *
     * @param $song Song
     */
    public function __construct(Model $song)
    {
        $this->song = $song;
        $this->path = $this->song->path . $this->song->filename;
        if (!file_exists($this->path)) {
            abort(404);
        }

        // Hard code the content type instead of relying on PHP's fileinfo()
        // or even Symfony's MIMETypeGuesser, since they appear to be wrong sometimes.
        if ($this->song->type == 'audio') {
            $this->contentType = 'audio/' . pathinfo($this->path, PATHINFO_EXTENSION);
        } else {
            $this->contentType = 'video/' . pathinfo($this->path, PATHINFO_EXTENSION);
        }

        // Turn off error reporting to make sure our stream isn't interfered.
        @error_reporting(0);
    }
}
