<?php

namespace Hierarchy\Alexa\Media\Streamers;

interface StreamerInterface
{
    /**
     * Stream the current song.
     */
    public function stream();
}
