<?php

namespace Hierarchy\Alexa\Media\Streamers;

use Illuminate\Database\Eloquent\Model;

class XSendFileStreamer extends Streamer implements StreamerInterface
{
    public function __construct(Model $song)
    {
        parent::__construct($song);
    }

    /**
     * Stream the current song using Apache's x_sendfile module.
     */
    public function stream()
    {
        header("X-Sendfile: {$this->path}");
        header("Content-Type: {$this->contentType}");
        header('Content-Disposition: inline; filename="' . basename($this->path) . '"');

        exit;
    }
}
