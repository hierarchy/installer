<?php

namespace Hierarchy\Alexa\Widgets\Test\Dummies;

use Hierarchy\Alexa\Widgets\AbstractWidget;

class Exception extends AbstractWidget
{
    public function run()
    {
        return 'Exception widget was executed instead of predefined php class';
    }
}
