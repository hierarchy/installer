<?php

namespace Hierarchy\Alexa\Widgets\Test\Support;

use Hierarchy\Alexa\Widgets\WidgetId;
use PHPUnit_Framework_TestCase;

class TestCase extends PHPUnit_Framework_TestCase
{
    public function tearDown()
    {
        WidgetId::reset();
    }

    public function ajaxUrl($widgetName, $widgetParams = [], $id = 1)
    {
        return '/arrilot/load-widget?' . http_build_query([
            'id' => $id,
            'name' => $widgetName,
            'params' => serialize($widgetParams),
        ]);
    }
}
