<?php

namespace Hierarchy\Alexa\Widgets\Misc;

use Exception;

class InvalidWidgetClassException extends Exception
{
    /**
     * Exception message.
     *
     * @var string
     */
    protected $message = 'Widget class must extend Hierarchy\Alexa\Widgets\AbstractWidget class';
}
