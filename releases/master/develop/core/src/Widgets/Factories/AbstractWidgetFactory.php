<?php

namespace Hierarchy\Alexa\Widgets\Factories;

use Hierarchy\Alexa\Package\Package;
use Hierarchy\Alexa\Widgets\AbstractWidget;
use Hierarchy\Alexa\Widgets\Contracts\ApplicationWrapperContract;
use Hierarchy\Alexa\Widgets\Misc\InvalidWidgetClassException;
use Hierarchy\Alexa\Widgets\Misc\ViewExpressionTrait;
use Hierarchy\Alexa\Widgets\WidgetId;

abstract class AbstractWidgetFactory
{
    use ViewExpressionTrait;

    const HINT_PATH_DELIMITER = '::';

    /**
     * Widget object to work with.
     *
     * @var AbstractWidget
     */
    protected $widget;

    /**
     * Widget configuration array.
     *
     * @var array
     */
    protected $widgetConfig;

    /**
     * The name of the widget being called.
     *
     * @var string
     */
    public $widgetName;

    /**
     * Array of widget parameters excluding the first one (config).
     *
     * @var array
     */
    public $widgetParams;

    /**
     * Array of widget parameters including the first one (config).
     *
     * @var array
     */
    public $widgetFullParams;

    /**
     * Laravel application wrapper for better testability.
     *
     * @var ApplicationWrapperContract;
     */
    public $app;

    /**
     * Hierarchy Package model
     * @var [type]
     */
    public $package;

    /**
     * Another factory that produces some javascript.
     *
     * @var JavascriptFactory
     */
    protected $javascriptFactory;

    /**
     * The flag for not wrapping content in a special container.
     *
     * @var bool
     */
    public static $skipWidgetContainer = false;

    /**
     * Constructor.
     *
     * @param ApplicationWrapperContract $app
     */
    public function __construct(ApplicationWrapperContract $app, Package $package)
    {
        $this->app = $app;
        $this->package = $package;
        $this->javascriptFactory = new JavascriptFactory($this);
    }

    /**
     * Magic method that catches all widget calls.
     *
     * @param string $widgetName
     * @param array  $params
     *
     * @return mixed
     */
    public function __call($widgetName, array $params = [])
    {
        array_unshift($params, $widgetName);

        return call_user_func_array([$this, 'run'], $params);
    }

    /**
     * Set class properties and instantiate a widget object.
     *
     * @param $params
     *
     * @throws InvalidWidgetClassException
     */
    protected function instantiateWidget(array $params = [])
    {
        WidgetId::increment();

        $this->widgetName = $this->parseFullWidgetNameFromString(array_shift($params));
        $this->widgetFullParams = $params;
        $this->widgetConfig = (array) array_shift($params);
        $this->widgetParams = $params;

        $rootNamespace = $this->app->config('widgets.default_namespace', $this->app->getNamespace() . 'Widgets');
        $fqcn = $rootNamespace . '\\' . $this->widgetName;

        $widgetClass = class_exists($fqcn) ? $fqcn : $this->widgetName;

        if (!is_subclass_of($widgetClass, 'Hierarchy\Alexa\Widgets\AbstractWidget')) {
            throw new InvalidWidgetClassException('Class "' . $widgetClass . '" must extend "Hierarchy\Alexa\Widgets\AbstractWidget" class');
        }

        $this->widget = new $widgetClass($this->widgetConfig);
    }

    /**
     * Convert stuff like 'profile.feedWidget' to 'Profile\FeedWidget'.
     *
     * @param $widgetName
     *
     * @return string
     */
    protected function parseFullWidgetNameFromString($widgetName)
    {
        if (class_exists($widgetName)) {
            return $widgetName;
        }
        $baseNamespace = '';
        $segments = explode(static::HINT_PATH_DELIMITER, $widgetName);
        $className = $widgetName;
        if (count($segments) > 1) {
            $className = $segments[1];
            $basePackageNamespace = $this->package->getNamespace();
            $packageNamespace = $package = $this->package->where('slug', $segments[0])->first();
            $baseNamespace = $basePackageNamespace . '\\' . $packageNamespace['namespace'] . '\\Current\\Widgets\\';
        }

        $namespaceGroup = explode('.', $className);
        if (count($namespaceGroup) > 1) {
            $class = [];
            foreach ($namespaceGroup as $key => $value) {
                $class[] = studly_case($value);
            }
            $className = implode('.', $class);
        }

        return $baseNamespace . studly_case(str_replace('.', '\\', $className));
    }

    /**
     * Wrap the given content in a container if it's not an ajax call.
     *
     * @param $content
     *
     * @return string
     */
    protected function wrapContentInContainer($content)
    {
        if (self::$skipWidgetContainer) {
            return $content;
        }

        $container = $this->widget->container();
        if (empty($container['element'])) {
            $container['element'] = 'div';
        }

        return '<' . $container['element'] . ' id="' . $this->javascriptFactory->getContainerId() . '" ' . $container['attributes'] . '>' . $content . '</' . $container['element'] . '>';
    }
}
