<?php

namespace Hierarchy\Alexa\Widgets;

class Facade extends \Illuminate\Support\Facades\Facade
{
    protected static function getFacadeAccessor()
    {
        return 'arrilot.widget';
    }
}
