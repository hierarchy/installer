<?php

namespace Hierarchy\Alexa\Widgets;

class AsyncFacade extends \Illuminate\Support\Facades\Facade
{
    protected static function getFacadeAccessor()
    {
        return 'arrilot.async-widget';
    }
}
