<?php

namespace Hierarchy\Alexa\Foundation\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

class HierarchyServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // register package
        $this->registerPackage();
        $this->registerAliases();
    }

    /**
     * [registerPackage description]
     * @return [type] [description]
     */
    public function registerPackage()
    {
        $this->app->register(\Collective\Html\HtmlServiceProvider::class);
        $this->app->register(\Hierarchy\Alexa\Widgets\ServiceProvider::class);
        $this->app->register(\Hierarchy\Alexa\Media\MediaServiceProvider::class);
        $this->app->register(\Hierarchy\Alexa\Support\SupportServiceProvider::class);
        $this->app->register(\Hierarchy\Alexa\Stylist\StylistServiceProvider::class);
        $this->app->register(\Hierarchy\Alexa\Console\ConsoleServiceProvider::class);
        $this->app->register(\Hierarchy\Alexa\Foundation\Theme\ThemeServiceProvider::class);
        $this->app->register(\Hierarchy\Alexa\Package\Providers\PackageServiceProvider::class);
    }

    public function registerAliases()
    {
        $aliasLoader = AliasLoader::getInstance();

        $aliasLoader->alias('Form', \Collective\Html\FormFacade::class);
        $aliasLoader->alias('Html', \Collective\Html\HtmlFacade::class);

        $aliasLoader->alias('Widget', \Hierarchy\Alexa\Widgets\Facade::class);
        $aliasLoader->alias('AsyncWidget', \Hierarchy\Alexa\Widgets\AsyncFacade::class);
    }
}
