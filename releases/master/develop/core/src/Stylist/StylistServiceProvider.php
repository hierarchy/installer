<?php
namespace Hierarchy\Alexa\Stylist;

use Config;
use Hierarchy\Alexa\Stylist\Html\ThemeHtmlBuilder;
use Hierarchy\Alexa\Stylist\Theme\Loader;
use Hierarchy\Alexa\Stylist\Theme\Stylist;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\AggregateServiceProvider;

class StylistServiceProvider extends AggregateServiceProvider
{
    /**
     * Stylist provides the HtmlServiceProvider for ease-of-use.
     *
     * @var array
     */
    protected $providers = [
        'Collective\Html\HtmlServiceProvider',
    ];

    /**
     * Registers the various bindings required by other packages.
     */
    public function register()
    {
        parent::register();

        $this->registerConfiguration();
        $this->registerStylist();
        $this->registerAliases();
        $this->registerThemeBuilder();
        $this->registerCommands();
    }

    /**
     * Boot the package, in this case also discovering any themes required by stylist.
     */
    public function boot()
    {
        $this->bootThemes();
    }

    /**
     * Once the provided has booted, we can now look at configuration and see if there's
     * any paths defined to automatically load and register the required themes.
     */
    protected function bootThemes()
    {
        $path = $this->app['config']->get('theme.path', public_path('themes'));

        $themePaths = $this->app['stylist']->discover($path);
        $this->app['stylist']->registerPaths($themePaths);

        $theme = $this->app['config']->get('theme.activate', null);

        if (!is_null($theme)) {
            Stylist::activate($theme);
        }
    }

    /**
     * Sets up the object that will be used for theme registration calls.
     */
    protected function registerStylist()
    {
        $this->app->singleton('stylist', function ($app) {
            return new Stylist(new Loader, $app);
        });
    }

    /**
     * Create the binding necessary for the theme html builder.
     */
    protected function registerThemeBuilder()
    {
        $this->app->singleton('stylist.theme', function ($app) {
            return new ThemeHtmlBuilder($app['html'], $app['url']);
        });
    }

    /**
     * Stylist class should be accessible from global scope for ease of use.
     */
    private function registerAliases()
    {
        $aliasLoader = AliasLoader::getInstance();

        $aliasLoader->alias('Stylist', 'Hierarchy\Alexa\Stylist\Facades\StylistFacade');
        $aliasLoader->alias('Theme', 'Hierarchy\Alexa\Stylist\Facades\ThemeFacade');

        $this->app->alias('stylist', 'Hierarchy\Alexa\Stylist\Theme\Stylist');
    }

    /**
     * Register the commands available to the package.
     */
    private function registerCommands()
    {
        $this->commands([
            'Hierarchy\Alexa\Stylist\Console\PublishAssetsCommand',
            'Hierarchy\Alexa\Stylist\Console\MakeThemeCommand',
            'Hierarchy\Alexa\Stylist\Console\ThemeInstallCommand',
        ]);
    }

    /**
     * Setup the configuration that can be used by stylist.
     */
    protected function registerConfiguration()
    {
        // $this->publishes([
        //     __DIR__ . '/../config/config.php' => config_path('stylist.php'),
        // ]);
    }

    /**
     * An array of classes that Stylist provides.
     *
     * @return array
     */
    public function provides()
    {
        return array_merge(parent::provides(), [
            'Stylist',
            'Theme',
        ]);
    }

}
