<?php

namespace Hierarchy\Alexa\Stylist\Console;

use GitWrapper\GitWrapper;
use Hierarchy\Alexa\Stylist\Theme\Stylist;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;

class ThemeInstallCommand extends Command
{
    /**
     * @var string
     */
    protected $name = 'theme:install';

    /**
     * @var string
     */
    protected $description = 'Create Theme Stucture.';

    protected $container = [];

    /**
     * [$wrapper description]
     * @var [type]
     */
    protected $wrapper;

    /**
     * Create a new command instance.
     *
     * @param Filesystem  $files
     * @param Package  $package
     */
    public function __construct(Filesystem $files, Stylist $stylist)
    {
        parent::__construct();

        $this->files = $files;
        $this->stylist = $stylist;
        $this->wrapper = new GitWrapper();
    }

    public function handle()
    {
        $this->container['name'] = strtolower($this->argument('name'));
        $this->container['git_url'] = $this->argument('git_url');
        $this->generate();
    }

    public function generate()
    {
        $steps = [
            'Generating folders...' => 'generateFolders',
            'Generating .gitkeep...' => 'cloneSources',
        ];

        $progress = new ProgressBar($this->output, count($steps));
        $progress->start();

        foreach ($steps as $message => $function) {
            $progress->setMessage($message);

            $this->$function();

            $progress->advance();
        }

        $progress->finish();
        $this->info("\nTheme install successfully.");
    }

    public function generateFolders()
    {

        if (!$this->files->isDirectory($this->stylist->basePath())) {
            $this->files->makeDirectory($this->stylist->basePath());
        }
        if ($this->files->isDirectory($this->stylist->getThemePath($this->container['name']))) {
            $this->files->delete($this->stylist->getThemePath($this->container['name']));
        }
        // $this->files->makeDirectory($this->stylist->getThemePath($this->container['name']));
    }

    /**
     * Generate defined package files.
     *
     * @return void
     */
    protected function cloneSources()
    {
        // path working update
        $pathCopy = $this->stylist->getThemePath($this->container['name']);

        // Optionally specify a private key other than one of the defaults.
        // $this->wrapper->setPrivateKey(base_path('/') . '../');

        // Clone a repo into `/path/to/working/copy`, get a working copy object.
        $git = $this->wrapper->clone($this->container['git_url'], $pathCopy);

        // git print output
        $this->info($git->getOutput());

        $this->callSilent('theme:publish');
    }

    /**
     * Developers can publish a specific theme should they wish.
     *
     * @return array
     */
    public function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'Name of the theme you'],
            ['git_url', InputArgument::REQUIRED, 'Git source theme'],
        ];
    }
}
