<?php
namespace Hierarchy\Alexa\Stylist\Console;

use Hierarchy\Alexa\Stylist\Theme\Stylist;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;

class MakeThemeCommand extends Command
{
    /**
     * @var string
     */
    protected $name = 'theme:make';

    /**
     * @var string
     */
    protected $description = 'Create Theme Stucture.';

    protected $container = [];

    /**
     * package folders to be created.
     *
     * @var array
     */
    protected $themeFolders = [
        // GUI folder struckture
        'assets',
        'assets/js',
        'assets/css',
        'assets/img',
        'resources',
        'resources/assets',
        'views',
        'views/widgets',
    ];

    /**
     * package files to be created.
     *
     * @var array
     */
    protected $themeFiles = [
        'views/welcome.blade.php',
        'theme.json',
        'elixir.json',
        'gulpfile.js',
        'package.json',
    ];

    /**
     * package stubs used to populate defined files.
     *
     * @var array
     */
    protected $themeStubs = [

        'welcome.stub',
        'manifest.stub',
        'elixir.stub',
        'gulpfile.stub',
        'package.stub',
    ];

    /**
     * Create a new command instance.
     *
     * @param Filesystem  $files
     * @param Package  $package
     */
    public function __construct(Filesystem $files, Stylist $stylist)
    {
        parent::__construct();

        $this->files = $files;
        $this->stylist = $stylist;
    }

    public function handle()
    {
        $this->container['name'] = strtolower($this->argument('name'));
        $this->stepOne();
    }

    public function stepOne()
    {
        $this->container['name'] = $this->ask('Please enter the name of the theme:', $this->container['name']);
        $this->container['description'] = $this->ask('Please enter the description of the package:', 'This is the description for the ' . $this->container['name'] . ' theme.');
        $this->container['type'] = $this->ask("Please enter the type theme [frontend/backend]:", 'frontend');

        $this->comment('You have provided the following manifest information:');
        $this->comment('Name:        ' . $this->container['name']);
        $this->comment('Description: ' . $this->container['description']);
        $this->comment('Type:      ' . $this->container['type']);

        if ($this->confirm('Do you wish to continue?')) {
            $this->comment('Thanks! That\'s all we need.');
            $this->comment('Now relax while your theme is generated for you.');

            $this->generate();
        } else {
            return $this->stepOne();
        }

        return true;
    }

    public function generate()
    {
        $steps = [
            'Generating folders...' => 'generateFolders',
            'Generating .gitkeep...' => 'generateGitkeep',
            'Generating files...' => 'generateFiles',
        ];

        $progress = new ProgressBar($this->output, count($steps));
        $progress->start();

        foreach ($steps as $message => $function) {
            $progress->setMessage($message);

            $this->$function();

            $progress->advance();
        }

        $progress->finish();
        $this->info("\npackage generated successfully.");
    }

    public function generateFolders()
    {

        if (!$this->files->isDirectory($this->stylist->basePath())) {
            $this->files->makeDirectory($this->stylist->basePath());
        }

        $this->files->makeDirectory($this->stylist->getThemePath($this->container['name']));
        foreach ($this->themeFolders as $folder) {
            $this->files->makeDirectory($this->stylist->getThemePath($this->container['name']) . $folder);
        }
    }

    /**
     * Generate defined package files.
     *
     * @return void
     */
    protected function generateFiles()
    {
        foreach ($this->themeFiles as $key => $file) {
            $file = $this->formatContent($file);

            $this->files->put($this->getDestinationFile($file), $this->getStubContent($key));
        }
    }

    /**
     * Generate .gitkeep files within generated folders.
     *
     * @return null
     */
    protected function generateGitkeep()
    {
        $dirs = $this->stylist->getThemePath($this->container['name']);
        foreach ($this->themeFolders as $folder) {
            $gitkeep = $dirs . $folder . '/.gitkeep';
            $this->files->put($gitkeep, '');
        }
    }

    /**
     * Replace placeholder text with correct values.
     *
     * @return string
     */
    protected function formatContent($content)
    {
        return str_replace(
            ['{{name}}', '{{description}}', '{{type}}'],
            [$this->container['name'], $this->container['description'], $this->container['type']],
            $content
        );
    }

    /**
     * Get destination file.
     *
     * @param  string $file
     * @return string
     */
    protected function getDestinationFile($file)
    {
        return $this->stylist->getThemePath($this->container['name']) . $this->formatContent($file);
    }

    /**
     * Get stub content by key.
     *
     * @param int  $key
     * @return string
     */
    protected function getStubContent($key)
    {
        return $this->formatContent($this->files->get(__DIR__ . '/stubs/' . $this->themeStubs[$key]));
    }

    /**
     * Developers can publish a specific theme should they wish.
     *
     * @return array
     */
    public function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'Name of the theme you'],
        ];
    }
}
