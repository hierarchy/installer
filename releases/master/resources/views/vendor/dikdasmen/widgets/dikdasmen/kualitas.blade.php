<!-- recentEvents -->
<div class="ui stackable grid">
	<div class="column">
		<div class="ui wrapper">
			<h4 class="ui marker header">
				Indeks Kualitas Kelengkapan Data (%)
				<a href="/app/dikdasmen"><i class="icon open folder outline"></i>LIHAT SEMUA</a>
			</h4>
			
			<div class="ui statistics">
				<div class="blue statistic">
					<div class="value" data-value="{{ $result->identitas_valid / 100 }}">
						<strong>{{ $result->identitas_valid }}</strong>
					</div>
					<div class="label">Sat. Pendidikan</div>
				</div>

				<div class="blue statistic">
					<div class="value" data-value="{{ $result->ptk_valid / 100 }}">
						<strong>{{ $result->ptk_valid }}</strong>
					</div>
					<div class="label">PTK</div>
				</div>

				<div class="blue statistic">
					<div class="value" data-value="{{ $result->pd_valid / 100 }}">
						<strong>{{ $result->pd_valid }}</strong>
					</div>
					<div class="label">Peserta Didik</div>
				</div>

				<div class="blue statistic">
					<div class="value" data-value="{{ $result->prasarana_valid / 100 }}">
						<strong>{{ $result->prasarana_valid }}</strong>
					</div>
					<div class="label">Sarana Prasarana</div>
				</div>

				<div class="blue statistic">
					<div class="value" data-value="{{ $result->persen / 100 }}">
						<strong>{{ $result->persen }}</strong>
					</div>
					<div class="label">Indeks</div>
				</div>
			</div>

			<div class="ui foot notes">
				SUMBER: 
				<a target="_blank" href="http://dapo.dikdasmen.kemdikbud.go.id/kelengkapan/1/280000">dapo.dikdasmen.kemdikbud.go.id</a>
				</div>
		</div>
	</div>
</div>