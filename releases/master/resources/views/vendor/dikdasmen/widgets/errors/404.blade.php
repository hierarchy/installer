<div class="ui stackable grid">
	<div class="column">
		<div class="ui wrapper">
			<h4 class="ui marker header">
				Bad Newtwork Exception
			</h4>
			<p>{{ $message }}</p>
		</div>
	</div>
</div>