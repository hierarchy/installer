@extends('layouts.base')

@section('mainStyle')
<link rel="stylesheet" href="{{ asset('packages/dikdasmen/css/dikdasmen.css') }}">
@endsection

@section('mainBanner')
<div class="ui slider" theme-part="prime-banner">
	<img class="ui image" src="{{ asset('packages/dikdasmen/img/education.png') }}" alt="">
</div>
@endsection

@section('mainContent')
<div class="ui stackable grid">
	<div class="column">
		<div class="ui wrapper">
			<h4 class="ui marker header">
				Index Kualitas Kelengkapan Data
				<a 	href="http://dapo.dikdasmen.kemdikbud.go.id/kelengkapan/2/280300"
					target="_blank">
					<i class="icon open folder outline"></i>LIHAT SUMBER
				</a>
			</h4>

			<div class="ui navigation segment">
				<div class="ui breadcrumb">
					<a href="{{ route('home.index') }}" class="section"><i class="icon home"></i></a>
					<span class="divider">/</span>
					<div class="active section">Kab. Tangerang</div>
				</div>
			</div>

			<div class="ui statistics">
				<div class="blue statistic">
					<div class="value" data-value="{{ $graph->identitas_valid / 100 }}">
						<strong>{{ $graph->identitas_valid }}</strong>
					</div>
					<div class="label">Sat. Pend</div>
				</div>

				<div class="blue statistic">
					<div class="value" data-value="{{ $graph->ptk_valid / 100 }}">
						<strong>{{ $graph->ptk_valid }}</strong>
					</div>
					<div class="label">PTK</div>
				</div>

				<div class="blue statistic">
					<div class="value" data-value="{{ $graph->pd_valid / 100 }}">
						<strong>{{ $graph->pd_valid }}</strong>
					</div>
					<div class="label">PST. Didik</div>
				</div>

				<div class="blue statistic">
					<div class="value" data-value="{{ $graph->prasarana_valid / 100 }}">
						<strong>{{ $graph->prasarana_valid }}</strong>
					</div>
					<div class="label">Sar. pras</div>
				</div>

				<div class="blue statistic">
					<div class="value" data-value="{{ $graph->persen / 100 }}">
						<strong>{{ $graph->persen }}</strong>
					</div>
					<div class="label">Indeks</div>
				</div>
			</div>

			<table class="ui very basic striped fixed single line description table">
				<thead>
					<tr>
						<th class="six wide">Wilayah</th>
						<th class="two wide" data-content="Satuan Pendidikan">(1)</th>
						<th class="two wide" data-content="PTK">(2)</th>
						<th class="two wide" data-content="Peserta Didik">(3)</th>
						<th class="two wide" data-content="Sarana dan Prasarana">(4)</th>
						<th class="two wide">Indeks</th>
					</tr>
				</thead>

				<tbody>
					@foreach ($result as $row)
					<tr>
						<td class="six wide">
							<a href="/app/dikdasmen/kelengkapan/{{ $row->id_level_wilayah }}/{{ trim($row->kode_wilayah) }}">
								{{ $row->nama }}
							</a>
						</td>
						<td class="two wide">{{ $row->identitas_valid }}</td>
						<td class="two wide">{{ $row->ptk_valid }}</td>
						<td class="two wide">{{ $row->pd_valid }}</td>
						<td class="two wide">{{ $row->prasarana_valid }}</td>
						<td class="two wide">{{ $row->persen }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			
		</div>
	</div>
</div>
@endsection

@section('mainScript')
<script>
	$(document).ready(function(){
		$('.ui.table thead tr th').popup({
			position: 'top center',
			variation: 'tiny inverted'
		});

		$('.statistic > .value').circleProgress({
			thickness: 12,
		    fill: { gradient: [['#0681c4', .5], ['#4ac5f8', .5]], gradientAngle: Math.PI / 4 }
		});
	});
</script>
@endsection