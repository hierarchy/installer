@extends('layouts.base')

@section('mainStyle')
<link rel="stylesheet" href="{{ asset('packages/dikdasmen/css/dikdasmen.css') }}">
@endsection

@section('mainBanner')
<div class="ui slider" theme-part="prime-banner">
	<img class="ui image" src="{{ asset('packages/dikdasmen/img/education.png') }}" alt="">
</div>
@endsection

@section('mainContent')
<div class="ui stackable grid">
	<div class="column">
		<div class="ui wrapper">

			<h4 class="ui marker header">
				Index Kualitas Kelengkapan Data
				<a 	href="http://dapo.dikdasmen.kemdikbud.go.id/sekolah/{{ $kode }}"
					target="_blank">
					<i class="icon open folder outline"></i>LIHAT SUMBER
				</a>
			</h4>

			<div class="ui navigation segment">
				<div class="ui breadcrumb">
					<a href="{{ route('home.index') }}" class="section"><i class="icon home"></i></a>
					<span class="divider">/</span>
					<a href="{{ route('app.dikdasmen.index') }}" class="section">Kab. Tangerang</a>
					<span class="divider">/</span>
					<a href="{{ route('app.dikdasmen.kelengkapan', [$result->id_level_wilayah_kecamatan, $result->kode_wilayah_kecamatan]) }}" class="section">{{ $result->induk_kecamatan }}</a>
					<span class="divider">/</span>
					<div class="active section">{{ $result->nama }}</div>
					<span class="divider">/</span>
					<div class="active section">{{ $result->sinkron_terakhir }}</div>
					
				</div>
			</div>

			<div class="ui gmap" id="map"></div>
			
			<table class="ui very basic stackable single line striped description table">
				<thead>
					<tr>
						<th class="four wide">PROFIL SEKOLAH</th>
						<th></th>
					</tr>
				</thead>

				<tbody>
					<tr>
						<td>NPSN</td>
						<td><span>:</span> {{ $result->npsn }}</td>
					</tr>

					<tr>
						<td>Status Sekolah</td>
						<td><span>:</span> {{ $result->status_sekolah }}</td>
					</tr>

					<tr>
						<td>Bentuk Pendidikan</td>
						<td><span>:</span> {{ $result->bentuk_pendidikan }}</td>
					</tr>

					<tr>
						<td>Alamat</td>
						<td><span>:</span> {{ $result->alamat_jalan }}</td>
					</tr>

					<tr>
						<td>RT / RW</td>
						<td><span>:</span> {{ $result->rt }} / {{ $result->rw }}</td>
					</tr>

					<tr>
						<td>Dusun</td>
						<td><span>:</span> {{ $result->nama_dusun or '-' }}</td>
					</tr>

					<tr>
						<td>Desa / Kelurahan</td>
						<td><span>:</span> {{ $result->desa_kelurahan }}</td>
					</tr>

					<tr>
						<td>Kecamatan</td>
						<td><span>:</span> {{ $result->induk_kecamatan }}</td>
					</tr>

					<tr>
						<td>Kabupaten</td>
						<td><span>:</span> {{ $result->induk_kabupaten }}</td>
					</tr>

					<tr>
						<td>Propinsi</td>
						<td><span>:</span> {{ $result->induk_provinsi }}</td>
					</tr>

					<tr>
						<td>Kode Post</td>
						<td><span>:</span> {{ $result->kode_pos }}</td>
					</tr>

					<tr>
						<td>Email</td>
						<td><span>:</span> {{ $result->email or '-' }}</td>
					</tr>

					<tr>
						<td>Website</td>
						<td><span>:</span> {{ $result->website or '-' }}</td>
					</tr>
				</tbody>
			</table>
			
			<table class="ui very basic stackable single line striped description table">
				<thead>
					<tr>
						<th class="four wide">DOKUMEN DAN PERIJINAN</th>
						<th></th>
					</tr>
				</thead>

				<tbody>
					<tr>
						<td>SK Pendirian Sekolah</td>
						<td><span>:</span> {{ $result->sk_pendirian_sekolah }}</td>
					</tr>

					<tr>
						<td>Tanggal SK Pendirian</td>
						<td><span>:</span> {{ $result->tanggal_sk_pendirian }}</td>
					</tr>

					<tr>
						<td>Status Kepemilikan</td>
						<td><span>:</span> {{ $result->status_kepemilikan }}</td>
					</tr>

					<tr>
						<td>SK Izin Operasional</td>
						<td><span>:</span> {{ $result->sk_izin_operasional }}</td>
					</tr>

					<tr>
						<td>Tanggal SK Izin Operasional</td>
						<td><span>:</span> {{ $result->tanggal_sk_izin_operasional }}</td>
					</tr>
				</tbody>
			</table>

			<table class="ui very basic stackable single line striped description table">
				<thead>
					<tr>
						<th class="four wide">DATA RINCI SEKOLAH</th>
						<th></th>
					</tr>
				</thead>

				<tbody>
					<tr>
						<td>Bersedia Menerima BOS</td>
						<td><span>:</span> {{ $result->partisipasi_bos }}</td>
					</tr>

					<tr>
						<td>Waktu Penyelenggaraan</td>
						<td><span>:</span> {{ $result->waktu_penyelenggaraan }}</td>
					</tr>

					<tr>
						<td>Sertifikasi ISO</td>
						<td><span>:</span> {{ $result->sertifikasi_iso }}</td>
					</tr>

					<tr>
						<td>Sumber Listrik</td>
						<td><span>:</span> {{ $result->sumber_listrik }}</td>
					</tr>

					<tr>
						<td>Daya Listrik</td>
						<td><span>:</span> {{ $result->daya_listrik }}</td>
					</tr>
				</tbody>
			</table>

			<table class="ui very basic stackable single line striped description table">
				<thead>
					<tr>
						<th>DATA PTK DAN PD</th>
						<th class="two wide">LAKI-LAKI</th>
						<th class="two wide">PEREMPUAN</th>
						<th class="two wide">TOTAL</th>
					</tr>
				</thead>

				<tbody>
					<tr>
						<td>Guru</td>
						<td class="two wide">{{ $result->guru_laki }}</td>
						<td class="two wide">{{ $result->guru_perempuan }}</td>
						<td class="two wide">{{ $result->guru_total }}</td>
					</tr>

					<tr>
						<td>Pegawai</td>
						<td class="two wide">{{ $result->pegawai_laki }}</td>
						<td class="two wide">{{ $result->pegawai_perempuan }}</td>
						<td class="two wide">{{ $result->pegawai_total }}</td>
					</tr>

					<tr>
						<td>Jumlah PTK (Guru & Pegawai)</td>
						<td class="two wide">{{ $result->guru_laki + $result->pegawai_laki }}</td>
						<td class="two wide">{{ $result->guru_perempuan + $result->pegawai_perempuan }}</td>
						<td class="two wide">{{ $result->guru_total + $result->pegawai_total }}</td>
					</tr>

					<tr>
						<td>Peserta Didik</td>
						<td class="two wide">{{ $result->pd_laki }}</td>
						<td class="two wide">{{ $result->pd_perempuan }}</td>
						<td class="two wide">{{ $result->pd_total }}</td>
					</tr>
				</tbody>
			</table>

			<div class="ui statistics">
				<div class="blue statistic">
					<div class="value" data-value="{{ $result->identitas_valid / 100 }}">
						<strong>{{ $result->identitas_valid }}</strong>
					</div>
					<div class="label">Sat. Pend</div>
				</div>

				<div class="blue statistic">
					<div class="value" data-value="{{ $result->ptk_valid / 100 }}">
						<strong>{{ $result->ptk_valid }}</strong>
					</div>
					<div class="label">PTK</div>
				</div>

				<div class="blue statistic">
					<div class="value" data-value="{{ $result->pd_valid / 100 }}">
						<strong>{{ $result->pd_valid }}</strong>
					</div>
					<div class="label">PST. Didik</div>
				</div>

				<div class="blue statistic">
					<div class="value" data-value="{{ $result->prasarana_valid / 100 }}">
						<strong>{{ $result->prasarana_valid }}</strong>
					</div>
					<div class="label">Sar. pras</div>
				</div>

				<div class="blue statistic">
					<div class="value" data-value="{{ $result->total_valid / 100 }}">
						<strong>{{ $result->total_valid }}</strong>
					</div>
					<div class="label">TOTAL</div>
				</div>
			</div>

			<div class="ui slider owl-theme owl-carousel">
				@foreach ($gallery as $photo)
				<div class="ui image">
					<img class="owl-lazy" data-src="{{ $photo['imgsrc'] }}" alt="">
					<h2 class="ui header">{{ $photo['filter'] }}</h2>
				</div>
				@endforeach
			</div>
		</div>
	</div>
</div>
@endsection

@section('mainScript')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBr-cm6UZvfjCxNwF9tbCkOUDIEEFvJofo&callback=initMap" async defer></script>
<script>
	function initMap() {
		var mapDiv = document.getElementById('map');
	    
	    var myLatLng = {
	    	lat: {{ $result->lintang }}, 
	    	lng: {{ $result->bujur }}
	    };

		var map = new google.maps.Map(mapDiv, {
			zoom: 17,
			center: myLatLng,
			scrollwheel: false
		});

		var marker = new google.maps.Marker({
			position: myLatLng,
			map: map,
			animation: google.maps.Animation.DROP,
			title: '{{ $result->nama }}'
		});
	}

	$(document).ready(function(){
		$('.ui.table thead tr th').popup({
			position: 'top center',
			variation: 'tiny inverted'
		});

		$('.owl-carousel').owlCarousel({
			items: 1,
			loop: true,
			nav: true,
			lazyLoad: true
		});

		$('.statistic > .value').circleProgress({
			thickness: 12,
		    fill: { gradient: [['#0681c4', .5], ['#4ac5f8', .5]], gradientAngle: Math.PI / 4 }
		});
	});
</script>
@endsection