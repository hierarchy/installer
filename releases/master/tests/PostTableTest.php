<?php

use Illuminate\Support\Facades\App;

// use Illuminate\Foundation\Testing\WithoutMiddleware;
// use Illuminate\Foundation\Testing\DatabaseMigrations;
// use Illuminate\Foundation\Testing\DatabaseTransactions;

class PostTableTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testPostInsert()
    {
        $this->seeInDatabase('languages', ['id' => 'en']);

        App::setlocale('en');

        $posts = factory(Package\Admin\Current\Entities\Post::class, 10)->create()->each(function ($post) {
            $post->meta()->save(factory(Package\Admin\Current\Entities\PostMeta::class)->make());
            $post->comments()->save(factory(Package\Admin\Current\Entities\PostComment::class)->make());
            $post->tag(['coba', 'hello', 'iya']);
        });

    }
}
