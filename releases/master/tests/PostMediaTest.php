<?php

use Illuminate\Support\Facades\App;
use Package\Admin\Current\Entities\Media;
use Package\Admin\Current\Entities\Post;
use Package\Admin\Current\Entities\PostMeta;

// use Illuminate\Foundation\Testing\WithoutMiddleware;
// use Illuminate\Foundation\Testing\DatabaseMigrations;
// use Illuminate\Foundation\Testing\DatabaseTransactions;

class PostMediaTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testMediaInsert()
    {
        App::setlocale('en');
        $post = factory(Post::class)->create();
        $attachments = factory(Media::class, 5)->create();
        $galleries = factory(Media::class)->create();

        $post->meta()->save(factory(PostMeta::class)->make());
        $post->attachments()->sync($attachments);
        $post->galleries()->sync([$galleries->id => ['type' => 'gallery']]);
    }
}
