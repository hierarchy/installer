<?php

use Illuminate\Support\Facades\App;
use Package\Admin\Current\Entities\Media;

// use Illuminate\Foundation\Testing\WithoutMiddleware;
// use Illuminate\Foundation\Testing\DatabaseMigrations;
// use Illuminate\Foundation\Testing\DatabaseTransactions;

class MediaTableTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testMediaInsert()
    {
        App::setlocale('en');

        $media = new Media;
        $media->meta->resolution = serialize(['height' => 200, 'width' => 200]);
        $media->fill([
            'filename' => str_random(10) . '.jpg',
            'extension' => '.jpg',
            'path' => public_path('upload'),
            'mimetype' => 'image/jpg',
            'filesize' => '12345',
            'type' => 'image',
            // trans
            'name' => 'Image Ajaib',
            'description' => 'Coba aja',
        ]);
        $media->save();

    }
}
