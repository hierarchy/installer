<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
 */

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(Package\Admin\Current\Entities\Media::class, function (Faker\Generator $faker) {
    return [
        'filename' => str_random(10) . '.jpg',
        'extension' => '.jpg',
        'path' => public_path('upload'),
        'mimetype' => 'image/jpg',
        'filesize' => $faker->randomNumber(4),
        'type' => 'image',
    ];
});

$factory->define(Package\Admin\Current\Entities\Post::class, function (Faker\Generator $faker) {
    return [
        'type' => $faker->randomElement(['article', 'post', 'page']),
        'author' => $faker->randomDigit,
        'publish' => 1,
        'parent_id' => $faker->randomElement([null, 1, 3]),
        'comment_status' => 1,

        // translate
        'title' => $faker->sentence(),
        'slug' => str_slug($faker->sentence(), '-'),
        'content' => $faker->text(),
    ];
});

$factory->define(Package\Admin\Current\Entities\PostMeta::class, function (Faker\Generator $faker) {
    return [
        'meta_key' => $faker->randomElement(['layout', 'size', 'page']),
        'meta_value' => $faker->sentence(),
    ];
});

$factory->define(Package\Admin\Current\Entities\PostComment::class, function (Faker\Generator $faker) {
    return [
        'author' => $faker->name,
        'author_email' => $faker->email,
        'author_url' => $faker->url,
        'author_ip' => $faker->ipv4,
        'content' => $faker->text(),
        'parent_id' => $faker->randomElement([null, 1, 3]),
        'publish' => $faker->randomElement([0, 1]),
    ];
});
