<?php

return [

    'default_namespace' => 'Package\Frontend\Current\Widgets',
    'use_jquery_for_ajax_calls' => false,
];
