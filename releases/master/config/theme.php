<?php

return [

    'admin_theme' => 'admin',

    'frontend_theme' => 'default',

    'path' => base_path('themes'),

    'activate' => null,
];
