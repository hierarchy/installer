<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application cache
    |--------------------------------------------------------------------------
    | Set this to true to use the cache decorators, this will greatly improve
    | the application speed and performance
     */
    'cache' => env('APP_CACHE', false),

    /*
    |--------------------------------------------------------------------------
    |
    |--------------------------------------------------------------------------
    |
     */
    'admin_prefix' => 'backend',

    /*
    |--------------------------------------------------------------------------
    | Upload path
    |--------------------------------------------------------------------------
    |
     */
    'upload' => [
        'path' => env('STORAGE_PATH', storage_path('app/upload')) . '/',
        'maxsize' => 1024,
    ],

    'docs' => [
        'ext' => '.md',
        'path' => base_path('docs'),
    ],
];
